package za.co.enerweb.toolbox.math;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class MathUtilsTest {
    private static final double EPSILON = 0.0001;

    @Test
    public void square() {
        assertEquals(25, MathUtils.square(5), EPSILON);
    }

    @Test
    public void percentToRatio() {
        assertEquals(0.15, MathUtils.percentToRatio(15.0), EPSILON);
    }

    @Test
    public void ratioToPercent() {
        assertEquals(15.0, MathUtils.ratioToPercent(0.15), EPSILON);
    }
}
