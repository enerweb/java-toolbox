package za.co.enerweb.toolbox.string;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class CsvBuilderTest {

    CsvBuilder csvBuilder = new CsvBuilder();

    @Test
    public void testBuilder() {
        csvBuilder.add("a")
            .add("b")
            .newline()
            .add(1)
            .add(new Integer(2))
            .newline()
            .add(3.0)
            .add(new Double(4.0))
            .newline()
            .add(true)
            .add(Boolean.FALSE)
            .newline();
        assertEquals("\"a\",\"b\"\n" +
            "1,2\n" +
            "3.0,4.0\n" +
            "true,false\n", csvBuilder.toString());
    }

    @Test
    public void indexValueXYCsv_1Col() {
        csvBuilder.addCaptions(asList("a"))
            .addDataXY(new double[][] {{1, 2}});
        assertEquals("\"a\"\n1.0\n2.0\n",
            csvBuilder.toString());
    }

    @Test
    public void indexValueXYCsv_1ColWithIndex() {
        csvBuilder.addCaptions(0, "i", asList("a"))
            .addDataXY(0, 0, new double[][] {{1, 2}});
        assertEquals("\"i\",\"a\"\n0,1.0\n1,2.0\n",
            csvBuilder.toString());
    }

    @Test
    public void indexValueXYCsv_1ColIndexLast() {
        csvBuilder.addCaptions(1, "i", asList("a"))
            .addDataXY(1, 0, new double[][] {{1, 2}});
        assertEquals("\"a\",\"i\"\n1.0,0\n2.0,1\n",
            csvBuilder.toString());
    }

    @Test
    public void indexValueXYCsv_1ColSkipRow() {
        csvBuilder.addCaptions(0, "i", asList("a"))
            .addDataXY(0, 1, new double[][] {{1, 2}});
        assertEquals("\"i\",\"a\"\n1,2.0\n",
            csvBuilder.toString());
    }

    @Test
    public void indexValueXYCsv() {
        csvBuilder.addCaptions(0, "i", asList("a", "b"))
            .addDataXY(0, 0, new double[][] { {1, 2}, {3, 4}});
        assertEquals("\"i\",\"a\",\"b\"\n0,1.0,3.0\n1,2.0,4.0\n",
            csvBuilder.toString());

    }
}
