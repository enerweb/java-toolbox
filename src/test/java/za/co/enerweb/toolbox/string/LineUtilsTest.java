package za.co.enerweb.toolbox.string;

import java.util.Enumeration;

import junit.framework.TestCase;

public class LineUtilsTest extends TestCase {

    public static void main(String[] args) {
        junit.textui.TestRunner.run(LineUtilsTest.class);
    }

    public LineUtilsTest(String arg0) {
        super(arg0);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    private void testGetLineEnumeration(String str, String[] expectedLines) {
        Enumeration<String> enumeration = LineUtils.getLineEnumeration(str);
        int lineNumber = 1;
        while (enumeration.hasMoreElements()) {
            String line = enumeration.nextElement();
            System.out.println(StringUtils.leftJustifyString(String
                .valueOf(lineNumber), 3)
                + ":" + line);
            assertEquals("line " + lineNumber, expectedLines[lineNumber - 1],
                line);
            lineNumber++;
        }
        assertEquals("equal lines", expectedLines.length, lineNumber - 1);
    }

    /*
     * Test method for
     * 'za.co.enerweb.krugepj.utils.string.Lines.getLineEnumeration(String)'
     */
    public void testGetLineEnumeration1() {
        testGetLineEnumeration("\r\n", new String[] {""});
    }

    public void testGetLineEnumeration2() {
        testGetLineEnumeration("abcd\ndef\n", new String[] {"abcd", "def"});
    }

    public void testGetLineEnumeration3() {
        testGetLineEnumeration("\r\nabcd\ndef\n\n\nxyz", new String[] {"",
            "abcd", "def", "", "", "xyz"});
    }
}
