package za.co.enerweb.toolbox.string;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class DeDuplicatorTest {

    private String separator = " ";
    private static final String BASE = "value";
    private DeDuplicator deDuper;
    private String firstAvailibleValue = BASE + separator + "3";

    public DeDuplicatorTest() {
        deDuper = new DeDuplicator() {
            public boolean isValueTaken(String value) {
                return dummyIsValueTaken(value);
            }
        };
    }

    @Test
    public void testBasic() {
        assertDeDupe(BASE + separator + 3, BASE);
        assertDeDupe(BASE + separator + 3, 1);
    }

    @Test(expected = RuntimeException.class)
    public void testMaxTries() {
        assertDeDupe(BASE + separator + 3, 4);
    }

    @Test
    public void testAlternativeSeparator() {
        deDuper.seperator(separator = "_");
        firstAvailibleValue = BASE + separator + "3";
        assertDeDupe(BASE + separator + 3, BASE);
        assertDeDupe(BASE + separator + 3, 1);
    }

    private void assertDeDupe(String expected, int index) {
        assertDeDupe(expected, BASE + separator + index);
    }

    private void assertDeDupe(String expected, String value) {
        assertEquals(expected, deDuper.deDupe(value));
    }

    public boolean dummyIsValueTaken(String value) {
        return !firstAvailibleValue.equals(value);
    }
}
