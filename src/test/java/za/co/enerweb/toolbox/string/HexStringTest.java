package za.co.enerweb.toolbox.string;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class HexStringTest {

    @Test
    public void testBytes2HexString() {
        String testString = "test data!";
        byte[] data = testString.getBytes();
        System.out.println(data);
        String hexString = HexString.bytes2HexString(data);
        System.out.println(hexString);
        assertEquals(hexString, "74657374206461746121");

        assertEquals("ff", HexString.bytes2HexString(new byte[] {(byte) 255}));
        assertEquals("ff00ff", HexString.bytes2HexString(new byte[] {
            (byte) 255, 0, (byte) 255}));
    }
}
