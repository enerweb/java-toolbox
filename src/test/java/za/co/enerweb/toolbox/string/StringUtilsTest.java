package za.co.enerweb.toolbox.string;

import static org.junit.Assert.assertEquals;
import static za.co.enerweb.toolbox.string.StringUtils.camelCaseToUnderscoreSeparated;
import static za.co.enerweb.toolbox.string.StringUtils.variableName2Caption;
import static za.co.enerweb.toolbox.string.StringUtils.variableName2Title;

import org.junit.Before;
import org.junit.Test;

public class StringUtilsTest {

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void testCamelCaseToUnderscoreSeparated() {
        assertEquals("Camel_Case_Camel",
            camelCaseToUnderscoreSeparated("CamelCaseCamel"));

        assertEquals("Camel_Case_Camel",
            camelCaseToUnderscoreSeparated("Camel Case Camel"));

        assertEquals("Camel_Case_Camel",
            camelCaseToUnderscoreSeparated("Camel.Case$Camel"));

        assertEquals("Camel1_Case2_Camel",
            camelCaseToUnderscoreSeparated("Camel1Case2Camel"));

        assertEquals("NON_CAMEL_CASE",
            camelCaseToUnderscoreSeparated("NON_CAMEL_CASE"));
    }

    @Test
    public void testVariableName2Caption() {
        assertEquals("Abc", variableName2Caption("abc"));
        assertEquals("Abc def", variableName2Caption("abc_def"));
        assertEquals("Abc def", variableName2Caption("ABC_DEF"));
        assertEquals("Abc def", variableName2Caption("abc.def"));
        assertEquals("Abc def", variableName2Caption("ABC.DEF"));
        assertEquals("Abc def", variableName2Caption("abcDef"));
        assertEquals("5 minutes", variableName2Caption("5minutes"));
        assertEquals("5 minutes", variableName2Caption("_5minutes"));
    }

    @Test
    public void testVariableName2Title() {
        assertEquals("Abc", variableName2Title("abc"));
        assertEquals("Abc Def", variableName2Title("abc_def"));
        assertEquals("Abc Def", variableName2Title("ABC_DEF"));
        assertEquals("Abc Def", variableName2Title("abc.def"));
        assertEquals("Abc Def", variableName2Title("ABC.DEF"));
        assertEquals("Abc Def", variableName2Title("abcDef"));
        assertEquals("5 Minutes", variableName2Title("5minutes"));
    }

    @Test
    public void double2HumanreadableString() {
        assertEquals("123.5", StringUtils
            .double2HumanreadableString(123.456, 1));
        assertEquals("0.46", StringUtils
            .double2HumanreadableString(0.456, 2));
        assertEquals("0.0", StringUtils
            .double2HumanreadableString(0.00123, 1));
    }


}
