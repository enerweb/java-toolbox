package za.co.enerweb.toolbox.string;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class JsonBeautifierTest {

    private JsonBeautifier jb = new JsonBeautifier();

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void abc() {
        assertEquals("abc", jb.beautify("abc"));
    }

    @Test
    public void abcTwice() {
        assertEquals("abc", jb.beautify("abc"));
        assertEquals("abc", jb.beautify("abc"));
    }

    @Test
    public void abcWithBraces() {
        assertEquals("{\n  abc\n}", jb.beautify("{abc}"));
    }

    @Test
    public void abcWith2levelsBraces() {
        assertEquals("{\n  abc{\n    def\n  }\n}", jb
            .beautify("{abc{def}}"));
    }

    @Test
    public void abcWithBracesAndQuotes() {
        assertEquals("{\n  \"abc\"\n}", jb.beautify("{\"abc\"}"));
    }

    @Test
    public void abcWithFields() {
        assertEquals("{\n  \"abc\"=123,\n  \"def\"=\"xyz\"\n}",
            jb.beautify("{\"abc\"=123,\"def\"=\"xyz\"}"));
    }

    @Test
    public void abcWithFieldsWithQuotedComma() {
        assertEquals("{\n  \"abc\"=123,\n  \"def\"=\"x,yz\"\n}",
            jb.beautify("{\"abc\"=123,\"def\"=\"x,yz\"}"));
    }

    @Test
    public void abcWithFieldsWithQuotedCommaWithEscapedQuote() {
        assertEquals("{\n  \"abc\"=123,\n  \"def\"=\"x\\\",yz\"\n}",
            jb.beautify("{\"abc\"=123,\"def\"=\"x\\\",yz\"}"));
    }
}
