package za.co.enerweb.toolbox.reflection;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import lombok.Data;
import lombok.SneakyThrows;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.google.common.collect.Sets;

@RunWith(value = Parameterized.class)
public class PropertyUtilsTest {

    private static final String INT_PROPERTY = "intProperty";
    private static final String I_PROPERTY = "iProperty";
    private Class<?> beanClass;

    public PropertyUtilsTest(Class<?> beanClass) {
       this.beanClass = beanClass;
    }

    @Parameters
    public static Collection<Object[]> data() {
      Object[][] data = new Object[][] { { NormalBean.class },
          { LombokBean.class } };
      return Arrays.asList(data);
    }

    public static class NormalBean {

        private int intProperty;
        private int iProperty;

        public int getIntProperty() {
            return intProperty;
        }

        public void setIntProperty(int a) {
            this.intProperty = a;
        }

        public int getIProperty() {
            return iProperty;
        }

        public void setIProperty(int a) {
            this.iProperty = a;
        }
    }

    @Data
    public static class LombokBean {
        private int intProperty;
        private int iProperty;
    }

    @Test
    public void testGetAllProperties() {
        Set<String> allProperties = new HashSet<String>(
            PropertyUtils.getAllProperties(beanClass));
        assertEquals(Sets.newHashSet(INT_PROPERTY, I_PROPERTY),
            allProperties);
    }

    @SneakyThrows
    @Test
    public void testGetAndSetAProperty() {
        Object bean = beanClass.newInstance();
        int value = 124;
        PropertyUtils.setProperty(bean, INT_PROPERTY, value);
        assertEquals(value, PropertyUtils.getProperty(bean, INT_PROPERTY));

        PropertyUtils.setProperty(bean, I_PROPERTY, value);
        assertEquals(value, PropertyUtils.getProperty(bean, I_PROPERTY));
    }

}
