package za.co.enerweb.toolbox.reflection;

import java.util.List;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class ClassHierarchyUtilsTest extends TestCase {

    //
    // constructors
    //

    public static Test suite() {
        return new TestSuite(ClassHierarchyUtilsTest.class);
    }

    //
    // unit test methods
    //

    public ClassHierarchyUtilsTest(String name) {
        super(name);
    }

    private void checkClasshierarchy(Class cls, int depth) {
        List classes = ClassHierarchyUtils.getSuperClasses(cls);

        // DEBUG:
        // for (Iterator it = classes.iterator(); it.hasNext(); ) {
        // Class c = (Class) it.next();
        // System.out.println(c.getName());
        // }
        // System.out.println();

        assertEquals("incorrect class hierarchy depth",
                 depth, classes.size());
    }

    //
    // other methods
    //

    public void testGetClosestCommonSuperClass() {
        Class result = ClassHierarchyUtils.getClosestCommonSuperClass(
            javax.swing.JFrame.class, javax.swing.JFrame.class);
        assertEquals(javax.swing.JFrame.class, result);

        result = ClassHierarchyUtils.getClosestCommonSuperClass(
            java.awt.Frame.class, javax.swing.JFrame.class);
        assertEquals(java.awt.Frame.class, result);

        result = ClassHierarchyUtils.getClosestCommonSuperClass(
            javax.swing.JFrame.class, java.awt.Frame.class);
        assertEquals(java.awt.Frame.class, result);

        result = ClassHierarchyUtils.getClosestCommonSuperClass(
            javax.swing.JButton.class, javax.swing.JTextField.class);
        assertEquals(javax.swing.JComponent.class, result);
    }

    public void testGetSuperClasses() {
        checkClasshierarchy(javax.swing.JFrame.class, 6);
        checkClasshierarchy(javax.swing.JButton.class, 6);
        checkClasshierarchy(javax.swing.JTextField.class, 6);
    }

}
