package za.co.enerweb.toolbox.reflection;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class ClassNameUtilsTest extends TestCase {

    //
    // constants
    //

    private static final String BYTE_PRIMITIVE;
    private static final String LONG_PRIMITIVE;
    private static final String OBJECT;
    private static final String BYTE_ARRAY;
    private static final String BYTE_ARRAY2;
    private static final String BYTE_ARRAY12;
    private static final String OBJECT_ARRAY;
    private static final String OBJECT_ARRAY2;

    //
    // init blocks
    //

    static {
        BYTE_PRIMITIVE = Byte.TYPE.getName();
        LONG_PRIMITIVE = Long.TYPE.getName();
        OBJECT = Object.class.getName();
        BYTE_ARRAY = new byte[] { }.getClass().getName();
        BYTE_ARRAY2 = new byte[][] { }.getClass().getName();
        BYTE_ARRAY12 =
            new byte[][][][][][][][][][][][] { }.getClass().getName();
        OBJECT_ARRAY = new Object[] { }.getClass().getName();
        OBJECT_ARRAY2 = new Object[][] { }.getClass().getName();
    }

    //
    // constructors
    //

    public static Test suite() {
        return new TestSuite(ClassNameUtilsTest.class);
    }

    //
    // unit test methods
    //

    public ClassNameUtilsTest(final String name) {
        super(name);
    }

    private void getArrayDimensions(final String descr,
        final int expectedDimensions,
        final String arrayBaseComponentType, final String epectedHumanReadable,
        final String className) throws ClassNotFoundException {
        // DEBUG:
        // String isArray = (ClassNameUtils.isArray(className))
        // ? "IS an Array"
        // : "NOT an Array";
        // System.out.println(descr + " : " + className + " : " + isArray +
        // " : "
        // + ClassNameUtils.getArrayDimensions(className) + " : "
        // + ClassNameUtils.getArrayBaseComponentType(className) + " : "
        // + ClassNameUtils.getHumanReadableClassName(className));

        assertEquals(descr + " is array?",
                 expectedDimensions > 0, ClassNameUtils.isArray(className));

        assertEquals(descr + " has incorrect dimensions",
                 expectedDimensions,
                 ClassNameUtils.getArrayDimensions(className));

        assertEquals(descr + " has incorrect arrayBaseComponentType",
                 arrayBaseComponentType,
                 ClassNameUtils.getArrayBaseComponentType(className));

        assertEquals(descr + " has incorrect HumanReadableClassName.",
            epectedHumanReadable,
            ClassNameUtils.getHumanReadableClassName(className));

        if (ClassNameUtils.isObject(className)) {
            Object obj = ClassNameUtils.instantiate(className);

            // DEBUG:
            // System.out.println("  -> " + className + " : "
            // + obj.getClass().getName());

            assertEquals(className, obj.getClass().getName());
        }
    }

    private void isObject(final String descr, final boolean expected,
        final String className) throws NotAnObjectException {
        // DEBUG:
        // String isObj = (ClassNameUtils.isObject(className))
        // ? "IS an Object"
        // : "NOT an Object";
        // System.out.println(descr + " : " + className + " : " + isObj);

        assertEquals(descr, expected, ClassNameUtils.isObject(className));

        if (expected) {
            ClassNameUtils.validateObject(className);
        } else {
            try {
                ClassNameUtils.validateObject(className);
                fail("validateObject should have failed for: " + className);
            } catch (NotAnObjectException ex) {
                // DEBUG:
                // System.out.println("  -> " + ex.getMessage());
            }
        }
    }

    public void testDeriveArrayType() {
        final String BRACKET = "[";

        String className = "java.lang.Integer";
        int dimensions = 0;
        String type = ClassNameUtils.deriveArrayType(className, dimensions);
        assertEquals("incorrect deriving of array type", className, type);
        assertEquals("dimension mismatch", -1, type.indexOf(BRACKET));

        dimensions = 1;
        type = ClassNameUtils.deriveArrayType(className, dimensions);
        assertEquals("incorrect deriving of array type",
                 "[Ljava.lang.Integer;", type);
        int begin = type.indexOf(BRACKET);
        int end = type.lastIndexOf(BRACKET) + 1;
        int brackets = type.substring(begin, end).length();
        assertEquals("dimension mismatch", dimensions, brackets);

        dimensions = 2;
        type = ClassNameUtils.deriveArrayType(className, dimensions);
        assertEquals("incorrect deriving of array type",
                 "[[Ljava.lang.Integer;", type);
        begin = type.indexOf(BRACKET);
        end = type.lastIndexOf(BRACKET) + 1;
        brackets = type.substring(begin, end).length();
        assertEquals("dimension mismatch", dimensions, brackets);
    }

    public void testGetArrayDimensions() throws ClassNotFoundException {
        getArrayDimensions("BYTE_PRIMITIVE", 0, BYTE_PRIMITIVE,
                       BYTE_PRIMITIVE, BYTE_PRIMITIVE);
        getArrayDimensions("LONG_PRIMITIVE", 0, LONG_PRIMITIVE,
                       LONG_PRIMITIVE, LONG_PRIMITIVE);

        getArrayDimensions("OBJECT", 0, OBJECT, OBJECT, OBJECT);

        getArrayDimensions("BYTE_ARRAY", 1, BYTE_PRIMITIVE,
                       BYTE_PRIMITIVE + "[]", BYTE_ARRAY);
        getArrayDimensions("OBJECT_ARRAY", 1, OBJECT,
                       OBJECT + "[]", OBJECT_ARRAY);

        getArrayDimensions("BYTE_ARRAY2", 2, BYTE_PRIMITIVE,
                       BYTE_PRIMITIVE + "[][]", BYTE_ARRAY2);
        getArrayDimensions("OBJECT_ARRAY2", 2, OBJECT,
                       OBJECT + "[][]", OBJECT_ARRAY2);

        getArrayDimensions("BYTE_ARRAY12", 12, BYTE_PRIMITIVE,
                       BYTE_PRIMITIVE + "[][][][][][][][][][][][]",
                       BYTE_ARRAY12);
    }

    //
    // other methods
    //

    public void testGetClassFromClassName() throws Exception {
        Class<?> cls = ClassNameUtils.getClass("int");
        assertNotNull("unable to obtain primitive type", cls);
        assertEquals("incorrect type obtained", Integer.TYPE, cls);

        cls = ClassNameUtils.getClass("double");
        assertNotNull("unable to obtain primitive type", cls);
        assertEquals("incorrect type obtained", Double.TYPE, cls);

        cls = ClassNameUtils.getClass("boolean");
        assertNotNull("unable to obtain primitive type", cls);
        assertEquals("incorrect type obtained", Boolean.TYPE, cls);

        cls = ClassNameUtils.getClass("long");
        assertNotNull("unable to obtain primitive type", cls);
        assertEquals("incorrect type obtained", Long.TYPE, cls);

        cls = ClassNameUtils.getClass("float");
        assertNotNull("unable to obtain primitive type", cls);
        assertEquals("incorrect type obtained", Float.TYPE, cls);

        cls = ClassNameUtils.getClass("short");
        assertNotNull("unable to obtain primitive type", cls);
        assertEquals("incorrect type obtained", Short.TYPE, cls);

        cls = ClassNameUtils.getClass("byte");
        assertNotNull("unable to obtain primitive type", cls);
        assertEquals("incorrect type obtained", Byte.TYPE, cls);

        cls = ClassNameUtils.getClass("char");
        assertNotNull("unable to obtain primitive type", cls);
        assertEquals("incorrect type obtained", Character.TYPE, cls);

        cls = ClassNameUtils.getClass("java.lang.Integer");
        assertNotNull("unable to obtain object type", cls);
        assertEquals("incorrect type obtained", Integer.class, cls);

        try {
            ClassNameUtils.getClass("XXX");
            fail("able to get non-existing class");
        } catch (ClassNotFoundException ex) { /* ignore */
        }

        try {
            ClassNameUtils.getClass(null);
            fail("able to get class by specifying null name parameter");
        } catch (IllegalArgumentException ex) { /* ignore */
        }

        try {
            ClassNameUtils.getClass("");
            fail("able to get class by specifying null name parameter");
        } catch (IllegalArgumentException ex) { /* ignore */
        }
    }

    public void testInstantiate() throws Exception {
        Object instance = ClassNameUtils.instantiate("java.lang.Integer");
        assertEquals("incorrect object instantiation",
                 Integer.class, instance.getClass());

        try {
            instance = ClassNameUtils.instantiate("int");
            assertEquals("incorrect primitive instantiation",
                   Integer.class, instance.getClass());
        } catch (ClassNotFoundException ex) {
            fail("unable to instantiate primitive class by "
                + "specifying class name");
        }
    }

    public void testIsObject() throws NotAnObjectException {
        isObject("BYTE_PRIMITIVE", false, BYTE_PRIMITIVE);
        isObject("LONG_PRIMITIVE", false, LONG_PRIMITIVE);

        isObject("OBJECT", true, OBJECT);

        isObject("BYTE_ARRAY", true, BYTE_ARRAY);
        isObject("OBJECT_ARRAY", true, OBJECT_ARRAY);

        isObject("BYTE_ARRAY2", true, BYTE_ARRAY2);
        isObject("OBJECT_ARRAY2", true, OBJECT_ARRAY2);
    }

}
