package za.co.enerweb.toolbox.reflection;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class ClassUtilsTest {

    private void instantiate(Class<?> cls) {
        Object obj = ClassUtils.instantiate(cls);

        // DEBUG:
        // System.out.println(cls.getName() + " : \"" + obj + "\"");

        assertNotNull(obj);
        if (cls.isPrimitive()) {
            assertEquals(ClassUtils.getWrapperClass(cls), obj.getClass());
        } else {
            assertEquals(cls, obj.getClass());
        }
    }

    @Test
    public void testGetWrapperClass() {
        assertEquals(ClassUtils.getWrapperClass(Integer.TYPE), Integer.class);
        assertEquals(ClassUtils.getWrapperClass(Double.TYPE), Double.class);
        assertEquals(ClassUtils.getWrapperClass(Boolean.TYPE), Boolean.class);
        assertEquals(ClassUtils.getWrapperClass(Long.TYPE), Long.class);
        assertEquals(ClassUtils.getWrapperClass(Float.TYPE), Float.class);
        assertEquals(ClassUtils.getWrapperClass(Short.TYPE), Short.class);
        assertEquals(ClassUtils.getWrapperClass(Byte.TYPE), Byte.class);
        assertEquals(ClassUtils.getWrapperClass(Character.TYPE),
            Character.class);
    }

    @Test
    public void testInstantiate() {
        instantiate(Integer.TYPE);
        instantiate(Integer.class);

        instantiate(Double.TYPE);
        instantiate(Double.class);

        instantiate(Boolean.TYPE);
        instantiate(Boolean.class);

        instantiate(Long.TYPE);
        instantiate(Long.class);

        instantiate(Float.TYPE);
        instantiate(Float.class);

        instantiate(Short.TYPE);
        instantiate(Short.class);

        instantiate(Byte.TYPE);
        instantiate(Byte.class);

        instantiate(Character.TYPE);
        instantiate(Character.class);

        instantiate(String.class);

        instantiate(java.util.Date.class);
        instantiate(java.sql.Date.class);

        instantiate(new Byte[0].getClass());
        instantiate(new Byte[0][].getClass());
        instantiate(new Byte[0][][][][][][].getClass());

        instantiate(new byte[0].getClass());
        instantiate(new byte[0][].getClass());
        instantiate(new byte[0][][][][][][].getClass());
    }

    @Test
    public void testHasMethod() {
        assertFalse(ClassUtils.hasMethod(String.class, "abc"));
        assertTrue(ClassUtils.hasMethod(String.class, "length"));
    }

    @Test
    public void testHasGetter() {
        assertFalse(ClassUtils.hasGetter(String.class, "abc"));
        assertTrue(ClassUtils.hasGetter(String.class, "bytes"));
    }
}
