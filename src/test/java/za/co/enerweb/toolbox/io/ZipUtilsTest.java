package za.co.enerweb.toolbox.io;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import junit.framework.TestCase;

public class ZipUtilsTest extends TestCase {

    public static void main(final String[] args) {
        junit.textui.TestRunner.run(ZipUtilsTest.class);
    }

    /**
     * Constructor for ZipUtilsTest.
     * @param arg0
     */
    public ZipUtilsTest(final String arg0) {
        super(arg0);
    }

    /*
     * @see TestCase#setUp()
     */
    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }

    /*
     * @see TestCase#tearDown()
     */
    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    public void testAddByteArray() throws IOException {
        // File zipfile = File.createTempFile("testZipRecursivelyFileFile",
        // ".zip");

        final String file_text = "This is a test file.";

        // ByteArrayOutputStream baos = new ByteArrayOutputStream;
        PipedOutputStream pipe_out = new PipedOutputStream();
        PipedInputStream pipe_in = new PipedInputStream(pipe_out);

        ZipUtils zip = new ZipUtils(pipe_out);
        final String test_path = "test_path";
        final String test_file_name = "test_file.txt";

        zip
            .addByteArray(file_text.getBytes(), test_path + "/"
                + test_file_name);
        ZipInputStream zis = new ZipInputStream(pipe_in);
        zip.cleanup();

        byte[] bytes = new byte[file_text.length() + 5];
        ZipEntry entry = zis.getNextEntry();
        assertEquals("Check if the name is right",
            entry.getName(), test_path + "/" + test_file_name);

        int bytes_read = zis.read(bytes);
        assertEquals("Read file is not the expected length: ", file_text
            .length()
            , bytes_read);
        String read_string = new String(bytes, 0, bytes_read);
        // System.out.println("file_text = '" + file_text + "'");
        // System.out.println("bytes     = '" + read_string + "'");
        assertEquals("Read file is not what is expected: ", file_text,
            read_string);
    }

    public void testAddStream() throws IOException {
        // File zipfile = File.createTempFile("testZipRecursivelyFileFile",
        // ".zip");

        final String file_text = "This is a test file.";

        // ByteArrayOutputStream baos = new ByteArrayOutputStream;
        PipedOutputStream pipe_out = new PipedOutputStream();
        PipedInputStream pipe_in = new PipedInputStream(pipe_out);

        ZipUtils zip = new ZipUtils(pipe_out);
        final String test_path = "test_path";
        final String test_file_name = "test_file.txt";

        zip.addStream(new ByteArrayInputStream(file_text.getBytes()),
            test_path + "/" + test_file_name);
        ZipInputStream zis = new ZipInputStream(pipe_in);
        zip.cleanup();

        byte[] bytes = new byte[file_text.length() + 5];
        ZipEntry entry = zis.getNextEntry();
        assertEquals("Check if the name is right",
            entry.getName(), test_path + "/" + test_file_name);

        int bytes_read = zis.read(bytes);
        assertEquals("Read file is not the expected length: ", file_text
            .length()
            , bytes_read);
        String read_string = new String(bytes, 0, bytes_read);
        // System.out.println("file_text = '" + file_text + "'");
        // System.out.println("bytes     = '" + read_string + "'");
        assertEquals("Read file is not what is expected: ", file_text,
            read_string);
    }

    /*
     * Class under test for void zipRecursively(File, File)
     */
    public void testZipRecursivelyFileFile() throws IOException {
        File tmpfile =
            File.createTempFile("testZipRecursivelyFileFile", ".txt");
        tmpfile.deleteOnExit();
        File zipfile =
            File.createTempFile("testZipRecursivelyFileFile", ".zip");
        zipfile.deleteOnExit();
        final String file_text = "This is a test file.";

        {
            FileOutputStream fos = new FileOutputStream(tmpfile);
            fos.write(file_text.getBytes());
            fos.flush();
            fos.close();
        }

        ZipUtils.zipRecursively(tmpfile, zipfile);
        // System.out.println( zipfile.getAbsolutePath() );

        FileInputStream fis = new FileInputStream(zipfile);
        ZipInputStream zis = new ZipInputStream(fis);
        byte[] bytes = new byte[file_text.length() + 5];
        ZipEntry entry = zis.getNextEntry();
        assertEquals("Check if the name is right",
            entry.getName(), tmpfile.getName());

        int bytes_read = zis.read(bytes);
        assertEquals("Read file is not the expected length: ", file_text
            .length()
            , bytes_read);
        String read_string = new String(bytes, 0, bytes_read);
        // System.out.println("file_text = '" + file_text + "'");
        // System.out.println("bytes     = '" + read_string + "'");
        assertEquals("Read file is not what is expected: ", file_text,
            read_string);

    }

    public void testUnZip() throws IOException {
        File htmldir = TempDir.createTempDir("html-dir", true);
        ZipUtils.unZip(ResourceUtils.getResourceAsStream("test/html.zip"),
            htmldir);
        assertEquals(4, htmldir.list().length);
    }
}
