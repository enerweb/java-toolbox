package za.co.enerweb.toolbox.io;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class FileUtilsTest {
    File srcDir = TempDir.createTempDir("src_dir");
    File destParentDir = TempDir.createTempDir("dest_parent_dir");
    File destDir = new File(destParentDir, "dest_dir");

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() {
        org.apache.commons.io.FileUtils.deleteQuietly(srcDir);
        org.apache.commons.io.FileUtils.deleteQuietly(destParentDir);
    }

    @Test
    public void testCopyDirectorySkipExistingFilesCopyTree()
        throws IOException {
        // create tree
        String level1FileName = "level1file";
        File srcLevel1File = new File(srcDir, level1FileName);
        String level1FileContent = "level 1 file content";
        org.apache.commons.io.FileUtils.writeStringToFile(srcLevel1File,
            level1FileContent);

        String level1DirName = "level1dir";
        File srcLevel1Dir = new File(srcDir, level1DirName);
        srcLevel1Dir.mkdir();

        String level2FileName = "level2file";
        File srcLevel2File = new File(srcLevel1Dir, level2FileName);
        String level2FileContent = "level 2 file content";
        org.apache.commons.io.FileUtils.writeStringToFile(srcLevel2File,
            level2FileContent);

        FileUtils.copyDirectorySkipExistingFiles(srcDir, destDir);

        // assert tree
        File destLevel1File = new File(destDir, level1FileName);
        assertTrue(destLevel1File.exists());
        assertEquals(level1FileContent,
            org.apache.commons.io.FileUtils.readFileToString(destLevel1File));

        File destLevel1Dir = new File(destDir, level1DirName);
        assertTrue(destLevel1Dir.exists());

        File destLevel2File = new File(destLevel1Dir, level2FileName);
        assertTrue(destLevel2File.exists());
        assertEquals(level2FileContent,
            org.apache.commons.io.FileUtils.readFileToString(destLevel2File));
    }

    @Test
    public void testCopyDirectorySkipExistingFilesSkipExisting()
        throws IOException {
        // create tree
        String level1FileName = "level1file";
        File srcLevel1File = new File(srcDir, level1FileName);
        String srcLevel1FileContent = "source level 1 file content";
        org.apache.commons.io.FileUtils.writeStringToFile(srcLevel1File,
            srcLevel1FileContent);

        File destLevel1File = new File(destDir, level1FileName);
        String destLevel1FileContent = "destination level 1 file content";
        org.apache.commons.io.FileUtils.writeStringToFile(destLevel1File,
            destLevel1FileContent);

        FileUtils.copyDirectorySkipExistingFiles(srcDir, destDir);

        // assert tree
        assertEquals(srcLevel1FileContent,
            org.apache.commons.io.FileUtils.readFileToString(srcLevel1File));
        assertTrue(destLevel1File.exists());
        assertEquals(destLevel1FileContent,
            org.apache.commons.io.FileUtils.readFileToString(destLevel1File));

    }
}
