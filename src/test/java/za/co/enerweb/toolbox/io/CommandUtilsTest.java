package za.co.enerweb.toolbox.io;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static za.co.enerweb.toolbox.io.CommandUtils.simpleExecute;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import lombok.extern.slf4j.Slf4j;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.matchers.JUnitMatchers;

import za.co.enerweb.toolbox.os.OsUtils;

/**
 * Test executing a system command
 */
@Slf4j
public class CommandUtilsTest {
    private static final String PING = "ping";
    private static final String LOCALHOST = "localhost";

    private File tempDir;

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        tempDir = TempDir.createTempDir();
    }

    @After
    public void tearDown() throws Exception {
        FileUtils.deleteQuietly(tempDir);
    }

    private String[] getPingArgs() {
        String[] pingArgs = null;
        if (OsUtils.isLinux()) {
            pingArgs = new String[] { PING, "-c", "1", LOCALHOST };
        } else if (OsUtils.isWindows()) {
            pingArgs = new String[] { PING, "-n", "1", LOCALHOST };
        } else {
            fail("could not run test for os: " + OsUtils.getOs());
        }
        return pingArgs;
    }

    @Test
    public void testExcecuteCommand() throws IOException, InterruptedException {
        CommandUtils cu = new CommandUtils();
        cu.setWorkingDir(tempDir); //not really needed
        cu.setCommandAndArgs(getPingArgs());
        cu.excecute();
        String stdout = cu.getStdOutOutput();
        log.debug("stdout:\n" + stdout);
        log.debug("stderr:\n" + cu.getStdErrOutput());
        Assert.assertThat(stdout, JUnitMatchers.containsString(PING));
        assertEquals(1, StringUtils.countMatches(stdout, PING));
        Assert.assertThat(stdout, JUnitMatchers.containsString(LOCALHOST));

        // run it again, and make sure the content is not duplicated,
        // i.e. the output should be reset between calls.
        cu.excecute();
        stdout = cu.getStdOutOutput();
        log.debug("stdout:\n" + stdout);
        assertEquals(1, StringUtils.countMatches(stdout, PING));
    }

    @Test
    public void testSimpleExcecuteCommand() throws IOException,
        InterruptedException {
        String stdout = simpleExecute(getPingArgs());
        log.debug("stdout:\n" + stdout);
        Assert.assertThat(stdout, JUnitMatchers.containsString(PING));
        assertEquals(1, StringUtils.countMatches(stdout, PING));
        Assert.assertThat(stdout, JUnitMatchers.containsString(LOCALHOST));
    }

    @Test
    public void testExcecuteCommandInShell() throws IOException,
        InterruptedException {
        CommandShell cs = new CommandShell();
        String cmd = "";
        if (OsUtils.isLinux()) {
            cmd = PING + " -c 1 " + LOCALHOST;
        } else if (OsUtils.isWindows()) {
            cmd = PING + " -n 1 " + LOCALHOST;
        } else {
            fail("could not run test for os: " + OsUtils.getOs());
        }
        cs.executeInShell(cmd);
        String stdout = cs.getStdOutOutput();
        log.debug("stdout:\n" + stdout);
        log.debug("stderr:\n" + cs.getStdErrOutput());
        Assert.assertThat(stdout, JUnitMatchers.containsString(PING));
        Assert.assertThat(stdout, JUnitMatchers.containsString(LOCALHOST));
        cs.stop();
    }

    @Test
    public void testFindExecutableOnPath() throws FileNotFoundException {
        String pingPath = CommandUtils.findFileInPath(
            tempDir, PING)
            .getAbsolutePath();
        assertTrue(pingPath.endsWith(PING));
        if (OsUtils.isLinux()) {
            assertEquals("/bin/ping", pingPath);
        } else {
            fail("could not run test for os: " + OsUtils.getOs());
        }
    }

}
