package za.co.enerweb.toolbox.io;

import static org.junit.Assert.assertTrue;

import java.io.File;

import org.junit.Test;

public class TempDirTest {

    private static final String TEST_TMP_DIR_GROUP = "test-tmp-dir-group";
    private static final String TEST_TEMP_DIR = "testTempDir";

    /**
     * Test method for
     * {@link za.co.enerweb.toolbox.io.TempDir #createTempDir(boolean)}.
     */
    @Test
    public void testCreateTempDirBoolean() {
        File tempdir = TempDir.createTempDir();
        // System.out.println(tempdir.getAbsolutePath());
        assertTrue(tempdir.exists());
    }

    /**
     * Test method for
     * {@link za.co.enerweb.toolbox.io.TempDir
     * #createTempDir(java.lang.String, boolean)}
     * .
     */
    @Test
    public void testCreateTempDirString() {
        File tempdir = TempDir.createTempDir(TEST_TEMP_DIR);
        // System.out.println(tempdir.getAbsolutePath());
        assertTrue(tempdir.exists());
        assertTrue(tempdir.getAbsolutePath().endsWith(TEST_TEMP_DIR));
    }

    @Test
    public void testCreateGroupedTempDirg() {
        File tempdir = TempDir.createGroupedTempDir(TEST_TMP_DIR_GROUP,
            TEST_TEMP_DIR, true);
        // System.out.println(tempdir.getAbsolutePath());
        assertTrue(tempdir.exists());
        assertTrue(tempdir.getName().startsWith(TEST_TEMP_DIR));
        assertTrue(tempdir.getParentFile().getName().startsWith(
            TEST_TMP_DIR_GROUP));
    }
}
