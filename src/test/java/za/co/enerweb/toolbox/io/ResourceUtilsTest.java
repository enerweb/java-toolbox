package za.co.enerweb.toolbox.io;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.util.Collection;

import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Test;

public class ResourceUtilsTest {

    private static final String TEST_RESOURCE = "test/resource.txt";
    private static final String TEST_RESOURCE_CONTENT = "howzit\n";

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
    }

    /**
     * Test method for
     * {@link za.co.enerweb.toolbox.io.ResourceUtils
     * #getResourceAsFile(java.lang.String)}
     * .
     * @throws IOException
     */
    @Test
    public void testGetResourceAsFile() throws IOException {
        File file = ResourceUtils.getResourceAsFile("log4j.properties");
        assertTrue(file.exists());
    }

    @Test
    public void testGetBareResourceAsFile() throws IOException {
        File file = ResourceUtils.getResourceAsFile(TEST_RESOURCE);
        assertTrue(file.getAbsolutePath().endsWith(TEST_RESOURCE));
        String fileContent =
            org.apache.commons.io.FileUtils.readFileToString(file);
        assertEquals(TEST_RESOURCE_CONTENT, fileContent);
    }

    /**
     * Test method for
     * {@link za.co.enerweb.toolbox.io.ResourceUtils
     * #getResourceAsStream(java.lang.String)}
     * .
     * @throws IOException
     */
    @Test
    public void testGetResourceAsStream() throws IOException {
        String fileContent = IOUtils.toString(
            ResourceUtils.getResourceAsStream(TEST_RESOURCE));
        assertEquals(TEST_RESOURCE_CONTENT, fileContent);
    }

    /**
     * Test method for
     * {@link za.co.enerweb.toolbox.io.ResourceUtils
     * #getResourceAsUrl(java.lang.String)}
     * .
     * @throws IOException
     */
    @Test
    public void testGetResourceAsUrl() throws IOException {
        String url = ResourceUtils.getResourceAsUrl(TEST_RESOURCE).toString();
        assertTrue(url.matches("file:/.*" + TEST_RESOURCE));
    }

    @Test
    public void testGetResourcesForPackage()
        throws IOException, ClassNotFoundException {
        Collection<String> resources = ResourceUtils.getResourcesForPackage(
            "test/");
        assertTrue(resources.contains(TEST_RESOURCE));
    }

    /**
     * Test method for
     * {@link za.co.enerweb.toolbox.io.ResourceUtils
     * #readResourceAsBytes(java.lang.String)}
     * .
     * @throws IOException
     */
    @Test
    public void testReadResourceAsBytes() throws IOException {
        byte[] fileContent = ResourceUtils.getResourceAsBytes(TEST_RESOURCE);
        assertArrayEquals(TEST_RESOURCE_CONTENT.getBytes(), fileContent);
    }

    /**
     * Test method for
     * {@link za.co.enerweb.toolbox.io.ResourceUtils
     * #getResourceAsString(java.lang.String)}
     * .
     * @throws IOException
     */
    @Test
    public void testReadResourceAsString() throws IOException {
        String fileContent = ResourceUtils.getResourceAsString(TEST_RESOURCE);
        assertEquals(TEST_RESOURCE_CONTENT, fileContent);
    }
}
