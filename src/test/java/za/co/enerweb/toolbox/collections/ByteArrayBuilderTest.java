package za.co.enerweb.toolbox.collections;

import static org.junit.Assert.assertArrayEquals;

import java.util.Arrays;

import lombok.extern.slf4j.Slf4j;

import org.junit.Test;

@Slf4j
public class ByteArrayBuilderTest {

    @Test
    public void build() {
        ByteArrayBuilder b = new ByteArrayBuilder()
            .append((byte) 1)
            .append(2)
            .append(new byte[] {3, 4})
            .prepend((byte) 0)
            .prepend(-1)
            .prepend(new byte[] {-3, -2});
        log.debug(b.toDebugString());
        byte[] bytes = b.build();
        log.debug(Arrays.toString(bytes));
        assertArrayEquals(new byte[]
        {-3, -2, -1, -1, -1, -1, 0, 1, 0, 0, 0, 2, 3, 4}, bytes);
    }

}
