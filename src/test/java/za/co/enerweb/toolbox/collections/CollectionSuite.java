package za.co.enerweb.toolbox.collections;

import junit.framework.Test;
import junit.framework.TestSuite;

public class CollectionSuite {

    public static Test suite() {
        TestSuite suite = new TestSuite("CollectionSuite");
        suite.addTest(CacheSetTestCase.suite());
        return suite;
    }

}
