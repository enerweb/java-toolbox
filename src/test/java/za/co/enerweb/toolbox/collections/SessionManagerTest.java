package za.co.enerweb.toolbox.collections;

import java.util.Collection;

import junit.framework.TestCase;

public class SessionManagerTest extends TestCase {
    SessionManager sm = new SessionManager();

    String SESSION_ID = "x123";
    Object SESSION_OBJECT = SESSION_ID + "'s object :)";
    String SESSION_ID2 = "x456";
    Object SESSION_OBJECT2 = new Integer(456);

    public SessionManagerTest(final String name) {
        super(name);
    }

    private void addSessionObject() {
        SessionObjectContainer soc = sm.getOrCreateSessionObject(SESSION_ID);
        soc.setObject(SESSION_OBJECT);
    }

    private void addSessionObject2() {
        SessionObjectContainer soc = sm.getOrCreateSessionObject(SESSION_ID2);
        soc.setObject(SESSION_OBJECT2);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    public void testDeleteSession() {
        addSessionObject();
        addSessionObject2();

        assertNotNull(sm.getExistingSession(SESSION_ID));
        assertNotNull(sm.getExistingSession(SESSION_ID2));

        sm.deleteSession(SESSION_ID);
        assertNull(sm.getExistingSession(SESSION_ID));

        sm.deleteSession(SESSION_ID);
        assertNull(sm.getExistingSession(SESSION_ID));

        assertNotNull(sm.getExistingSession(SESSION_ID2));
        sm.deleteSession(SESSION_ID2);
        assertNull(sm.getExistingSession(SESSION_ID2));
    }

    public void testDeleteStaleSessionsInt() throws InterruptedException {
        SessionObjectContainer soc;
        addSessionObject();

        assertNotNull(sm.getExistingSession(SESSION_ID));
        sm.deleteStaleSessions(1);
        assertNotNull(sm.getExistingSession(SESSION_ID));
        Thread.sleep(1);
        Collection deleted = sm.deleteStaleSessions(0);
        assertTrue(deleted.contains(SESSION_ID));
        assertNull(sm.getExistingSession(SESSION_ID));
    }

    public void testDeleteStaleSessionsIntInt() throws InterruptedException {
        addSessionObject();
        addSessionObject2();

        assertNotNull(sm.getExistingSession(SESSION_ID));
        sm.deleteStaleSessions(0, 1);
        assertNotNull(sm.getExistingSession(SESSION_ID));
        Thread.sleep(1100);

        assertEquals(sm.getExistingSession(SESSION_ID2).getObject(),
            SESSION_OBJECT2); // access session2

        Collection deleted = sm.deleteStaleSessions(0, 1);
        assertTrue(deleted.contains(SESSION_ID));
        assertNull(sm.getExistingSession(SESSION_ID));

        assertEquals(sm.getExistingSession(SESSION_ID2).getObject(),
            SESSION_OBJECT2);
    }

    public void testGetAllSessionIds() {
        Collection sessionIds = sm.getAllSessionIds();
        assertFalse(sessionIds.contains(SESSION_ID));

        addSessionObject();

        sessionIds = sm.getAllSessionIds();
        assertTrue(sessionIds.contains(SESSION_ID));

        addSessionObject2();

        sessionIds = sm.getAllSessionIds();
        assertTrue(sessionIds.contains(SESSION_ID));
        assertTrue(sessionIds.contains(SESSION_ID2));
    }

    public void testGetExistingSession() {
        assertNull(sm.getExistingSession(SESSION_ID));

        SessionObjectContainer soc = sm.getOrCreateSessionObject(SESSION_ID);
        assertNull(soc.getObject());
        soc.setObject(SESSION_OBJECT);

        assertEquals("was object stored", SESSION_OBJECT,
            sm.getExistingSession(SESSION_ID).getObject());
    }

    public void testGetOrCreateSessionObject() {
        SessionObjectContainer soc = sm.getOrCreateSessionObject(SESSION_ID);
        assertNull(soc.getObject());
        soc.setObject(SESSION_OBJECT);

        assertEquals("was object stored", SESSION_OBJECT,
            sm.getOrCreateSessionObject(SESSION_ID).getObject());
    }

}
