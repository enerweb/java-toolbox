package za.co.enerweb.toolbox.collections;

import static org.junit.Assert.assertArrayEquals;
import static za.co.enerweb.toolbox.collections.ArrayUtils.join;
import static za.co.enerweb.toolbox.collections.ArrayUtils.joinBackToFront;

import org.junit.Test;

public class ArrayUtilsTest {

    @Test
    public void testAppend() {
        String[] actual = ArrayUtils.append(String.class, new String[] { "a",
            "b" }, "c");
        assertArrayEquals(new String[] { "a", "b", "c" }, actual);
    }

    @Test
    public void testAppendEmpty() {
        String[] actual = ArrayUtils.append(String.class,
            new String[] {}, "c");
        assertArrayEquals(new String[] { "c" }, actual);
    }

    @Test
    public void testJoin() {
        String[] actual = join(String.class,
            new String[] { "a", "b" }, new String[] { "c", "d" });
        assertArrayEquals(new String[] { "a", "b", "c", "d" }, actual);
    }

    @Test
    public void testJoinBackToFront() {
        String[] actual = joinBackToFront(String.class,
            new String[] {
            "c", "d" }, "a", "b");
        assertArrayEquals(new String[] { "a", "b", "c", "d" }, actual);
    }

    @Test
    public void testSubArray() {
        String[] src = new String[] { "a", "b", "c" };
        assertArrayEquals(new String[] {}, ArrayUtils.subArray(
            String.class, src, 0, 0));
        assertArrayEquals(new String[] {}, ArrayUtils.subArray(String.class,
            src, 1, 1));
        assertArrayEquals(new String[] {}, ArrayUtils.subArray(String.class,
            src, 2, 2));

        assertArrayEquals(new String[] { "a" }, ArrayUtils.subArray(
            String.class, src, 0, 1));
        assertArrayEquals(new String[] { "b" }, ArrayUtils.subArray(
            String.class, src, 1, 2));
        assertArrayEquals(new String[] { "c" }, ArrayUtils.subArray(
            String.class, src, 2, 3));

        assertArrayEquals(new String[] { "a", "b" }, ArrayUtils.subArray(
            String.class, src, 0, 2));
        assertArrayEquals(new String[] { "b", "c" }, ArrayUtils.subArray(
            String.class, src, 1, 3));

        assertArrayEquals(new String[] { "a", "b", "c" }, ArrayUtils.subArray(
            String.class, src, 0, 3));
    }
}
