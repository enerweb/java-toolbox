package za.co.enerweb.toolbox.collections;

import junit.framework.TestCase;

public class BufferTest extends TestCase {
    Buffer testBuffer = new Buffer(5);

    public BufferTest(String name) {
        super(name);
    }

    private void assertSize(String message, int i) {
        System.out.println(message + " testCache.size = " + testBuffer.size());
        assertEquals(message, i, testBuffer.size());
    }

    private void put(int i) {
        Integer I = new Integer(i);
        System.out.println("Adding " + i);
        testBuffer.add(I);
    }

    @Override
    protected void setUp() throws Exception {

    }

    public void testRemovalOfEccessValues() {
        int i = 0;
        while (i < 5) {
            put(i++);
            assertSize("Initial map size grows:", i);
        }

        while (i < 10) {
            put(i++);
            assertSize("Buffer doesn't grow too far:", 5);
        }
        assertTrue("buffer", testBuffer.contains(new Integer(5)));
        assertTrue("buffer", testBuffer.contains(new Integer(6)));
        assertTrue("buffer", testBuffer.contains(new Integer(7)));
        assertTrue("buffer", testBuffer.contains(new Integer(8)));
        assertTrue("buffer", testBuffer.contains(new Integer(9)));

        Buffer testBuffer2 = new Buffer(2);
        testBuffer2.addAll(testBuffer);
        assertEquals("buffer2", 2, testBuffer2.size());
        assertTrue("buffer2", testBuffer2.contains(new Integer(8)));
        assertTrue("buffer2", testBuffer2.contains(new Integer(9)));
    }

}
