/*
 * Created on 2007/04/19
 */
package za.co.enerweb.toolbox.collections;

import java.util.Arrays;
import java.util.List;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import junitx.framework.ArrayAssert;

/**
 * @author Elardus Viljoen
 */
public class CacheSetTestCase extends TestCase {

    //
    // constants
    //

    private static final String AGNETA = "agneta";
    private static final String BENNIE = "bennie";
    private static final String BJORN = "bjorn";
    private static final String ANAFRIDE = "anafride";

    //
    // constructors
    //

    public static Test suite() {
        return new TestSuite(CacheSetTestCase.class);
    }

    //
    // unit test methods
    //

    public CacheSetTestCase(String name) {
        super(name);
    }

    /**
     * test addAll overriding
     */
    public void testAddAll() {
        CacheSet cache = new CacheSet(2);
        cache.add(AGNETA);
        Object[] array = {BENNIE, BJORN};
        List list = Arrays.asList(array);
        cache.addAll(list);
        assertEquals("cache size wrong", 2, cache.size());
        ArrayAssert.assertEquals("cache entry array wrong",
                             array, cache.toArray());
    }

    /**
     * test double cache entry
     */
    public void testDouble() {
        CacheSet cache = new CacheSet(2);
        cache.add(AGNETA);
        cache.add(BENNIE);
        assertEquals("cache size wrong", 2, cache.size());
    }

    /**
     * test double cache entry with capacity reached
     */
    public void testDoubleCapacity() {
        CacheSet cache = new CacheSet(2);
        cache.add(AGNETA);
        cache.add(BENNIE);
        cache.add(BJORN);
        assertEquals("cache size wrong", 2, cache.size());
    }

    /**
     * test double cache entry with manual removal
     */
    public void testDoubleRemoval() {
        CacheSet cache = new CacheSet(2);
        cache.add(AGNETA);
        cache.add(BENNIE);
        cache.remove(AGNETA);
        cache.remove(BENNIE);
        assertEquals("cache size wrong", 0, cache.size());
    }

    /**
     * test negative cache capacity
     */
    public void testNegative() {
        try {
            CacheSet cache = new CacheSet(-1);
            fail("able to set negative capacity");
        } catch (Exception ex) { /* ignore */
        }
    }

    /**
     * test whether order of entries is preserved
     */
    public void testOrder() {
        CacheSet cache = new CacheSet(4);
        cache.add(AGNETA);
        cache.add(BENNIE);
        cache.add(BJORN);
        cache.add(ANAFRIDE);
        Object[] expected = {AGNETA, BENNIE, BJORN, ANAFRIDE};
        ArrayAssert.assertEquals("cache entry array wrong",
                             expected, cache.toArray());
    }

    /**
     * test single cache entry
     */
    public void testSingle() {
        CacheSet cache = new CacheSet(1);
        cache.add(AGNETA);
        assertEquals("cache size wrong", 1, cache.size());
    }

    /**
     * test single cache entry with capacity reached
     */
    public void testSingleCapacity() {
        CacheSet cache = new CacheSet(1);
        cache.add(AGNETA);
        cache.add(BENNIE);
        assertEquals("cache size wrong", 1, cache.size());
    }

    /**
     * test whether order of entries is updated when an existing entry is
     * added again
     */
    public void testUpdatedOrder() {
        CacheSet cache = new CacheSet(4);
        assertEquals("cache capacity wrong", 4, cache.getCapacity());
        cache.add(AGNETA);
        cache.add(BENNIE);
        cache.add(BJORN);
        cache.add(AGNETA);
        assertEquals("cache size wrong", 3, cache.size());
        Object[] expected = {BENNIE, BJORN, AGNETA};
        ArrayAssert.assertEquals("cache entry array wrong",
                             expected, cache.toArray());
    }

    //
    // other methods
    //

    /**
     * test zero cache capacity
     */
    public void testZero() {
        try {
            CacheSet cache = new CacheSet(0);
            fail("able to set zero capacity");
        } catch (Exception ex) { /* ignore */
        }
    }

}
