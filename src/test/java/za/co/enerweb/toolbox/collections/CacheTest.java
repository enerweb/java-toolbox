package za.co.enerweb.toolbox.collections;

import junit.framework.TestCase;

public class CacheTest extends TestCase {
    Cache testCache = new Cache(5);

    public CacheTest(String name) {
        super(name);
    }

    private void assertSize(String message, int i) {
        System.out.println(message + " testCache.size = " + testCache.size());
        assertEquals(message, i, testCache.size());
    }

    private void put(int i) {
        Integer I = new Integer(i);
        System.out.println("Adding " + i);
        testCache.put(I, I);
    }

    @Override
    protected void setUp() throws Exception {

    }

    public void testRemovalOfEccessValues() {
        int i = 0;
        while (i < 5) {
            put(i++);
            assertSize("Initial map size grows:", i);
        }

        while (i < 10) {
            put(i++);
            assertSize("Map doesn't grow too far:", 5);
        }
    }

}
