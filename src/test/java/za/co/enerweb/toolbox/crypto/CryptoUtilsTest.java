package za.co.enerweb.toolbox.crypto;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static za.co.enerweb.toolbox.crypto.CryptoUtils.generateKeyPairRSA;

import java.security.Key;
import java.security.KeyPair;

import lombok.SneakyThrows;

import org.apache.commons.lang.ArrayUtils;
import org.junit.Test;

public class CryptoUtilsTest {
    private static String SECRET_MESSAGE = "secret message";
    private static final byte[] SECRET_MESSAGE_BYTES = SECRET_MESSAGE.getBytes();
    private static KeyPair keyPair = generateKeyPairRSA();

    @SneakyThrows
    @Test
    public void sha256() {
        assertEquals(
            "af50a680838420111be6c02c04f384bd668e4690b3b3b8e655ce899bc52fac4f",
            CryptoUtils.sha256(
                // FileUtils.readBytes("/tmp/amanica/sha256me")
                "Dummy data for hashing please!\n".getBytes("UTF-8")
                ));
    }

    @SneakyThrows
    @Test
    public void encryptAes() {
        String keyStr = "sercret key23456";

        byte[] encData = CryptoUtils.encryptAes(SECRET_MESSAGE_BYTES,keyStr.getBytes());
        byte[] decData = CryptoUtils.decryptAes(encData, keyStr.getBytes());

        assertFalse(SECRET_MESSAGE.equals(new String(encData)));
        assertTrue(SECRET_MESSAGE.equals(new String(decData)));
    }

    @SneakyThrows
    @Test
    public void encryptBlowfish() {
        Key key = CryptoUtils.getBlowfishSyncKey();

        byte[] encData = CryptoUtils.encryptBlowfish(SECRET_MESSAGE_BYTES, key);
        byte[] decData = CryptoUtils.decryptBlowfish(encData, key);
        byte[] decData2 =
            CryptoUtils.decryptBlowfish(encData, key.getEncoded());

        assertFalse(SECRET_MESSAGE.equals(new String(encData)));
        assertTrue(SECRET_MESSAGE.equals(new String(decData)));
        assertTrue(SECRET_MESSAGE.equals(new String(decData2)));
    }

    @SneakyThrows
    @Test
    public void encryptRsaPrivate() {
        byte[] encData =
            CryptoUtils.encryptRsa(SECRET_MESSAGE_BYTES, keyPair.getPrivate()
                .getEncoded(), true);
        byte[] decData =
            CryptoUtils.decryptRsa(encData, keyPair.getPublic().getEncoded(),
                false);

        assertFalse(SECRET_MESSAGE.equals(new String(encData)));
        assertTrue(SECRET_MESSAGE.equals(new String(decData)));
    }

    @SneakyThrows
    @Test
    public void encryptRsaPublic() {
        byte[] encData =
            CryptoUtils.encryptRsa(SECRET_MESSAGE_BYTES, keyPair.getPublic()
                .getEncoded(), false);
        byte[] decData =
            CryptoUtils.decryptRsa(encData, keyPair.getPrivate().getEncoded(),
                true);

        assertFalse(SECRET_MESSAGE.equals(new String(encData)));
        assertTrue(SECRET_MESSAGE.equals(new String(decData)));
    }

    @SneakyThrows
    @Test
    public void sign() {
        byte[] signiature =
            CryptoUtils.sign(SECRET_MESSAGE_BYTES, keyPair.getPrivate());

        assertFalse(CryptoUtils.verifySigniature("invalid data".getBytes(),
            keyPair.getPublic(), signiature));
        assertTrue(CryptoUtils.verifySigniature(SECRET_MESSAGE_BYTES,
            keyPair.getPublic(), signiature));

        ArrayUtils.reverse(signiature);
        assertFalse("Invalid signiature",
            CryptoUtils.verifySigniature(SECRET_MESSAGE_BYTES,
                keyPair.getPublic(), signiature));
    }

    @SneakyThrows
    @Test
    public void base64() {
        String base64 = CryptoUtils.encodeBase64(SECRET_MESSAGE_BYTES);
        assertEquals("c2VjcmV0IG1lc3NhZ2U=", base64);
        assertEquals(SECRET_MESSAGE, new String(
            CryptoUtils.decodeBase64(base64)));
    }
}
