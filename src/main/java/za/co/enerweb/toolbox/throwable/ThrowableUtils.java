package za.co.enerweb.toolbox.throwable;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import lombok.val;
import za.co.enerweb.toolbox.string.StringUtils;

/**
 * - Only trust something as far as you can throw it.
 */
public class ThrowableUtils {

    /**
     * Get a nice list of unique messages going down the cause stack.
     */
    public static List<String> getMessages(Throwable t) {
        val ret = new ArrayList<String>(6);
        Throwable curThrowable = t;
        if ((t instanceof RemoteException) && t.getCause() != null) {
            // skip nasty useless non-application exception message
            curThrowable = t.getCause();
        }
        val capturedMessages = new HashSet<String>();
        while (curThrowable != null) {
            String message = curThrowable.getLocalizedMessage();
            if (message == null) {
                message = curThrowable.getMessage();
            }
            message = org.apache.commons.lang.StringUtils.trimToEmpty(message)
                + " - " + StringUtils.variableName2Caption(
                    curThrowable.getClass().getSimpleName()
                        .replaceAll("Exception", ""));

            if (!capturedMessages.contains(message)) {
                ret.add(message);
                capturedMessages.add(message);
            }
            curThrowable = curThrowable.getCause();
        }
        // XXX: maybe de-dupe things that start with each other..
        return ret;
    }

    public static String getMessagesAsString(Throwable t) {
        return StringUtils.join(getMessages(t), "\n");
    }
}
