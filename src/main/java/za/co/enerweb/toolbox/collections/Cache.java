package za.co.enerweb.toolbox.collections;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * This Hashmap type class, is very useful for caching objects. It will only
 * fill up the cache until as specific size, and then start to removed unused or
 * old values to make room for new ones. Ideal way to store expensive-to-load
 * objects, which we don't want to load every time a request comes in. In stead
 * we'd like to keep the most recent used ones in memory.
 * @author Marius Kruger
 */
public class Cache<K, V> extends LinkedHashMap<K, V> {

    private static final long serialVersionUID = 434838081597725461L;

    private static final boolean ACCESS_ORDER = true;
    private final int maxCacheSize;

    /**
     * Initializes the maxCacheSize to 100
     */
    public Cache() {
        this(100);
    }

    /**
     * Creates the cache at the maxCacheSize, and then keeps it at that size
     * @param maxCacheSize
     */
    public Cache(final int maxCacheSize) {
        super(maxCacheSize, 0.99f, ACCESS_ORDER);
        this.maxCacheSize = maxCacheSize;
    }

    /**
     * Creates the cache with the initialCapacity, and then grows it until it
     * reaches maxCacheSize, this constructor will cause a possable wasted
     * memory of maxCacheSize * 0.10 entries.
     * @param initialCapacity
     * @param maxCacheSize
     */
    public Cache(final int initialCapacity, final int maxCacheSize) {
        super(initialCapacity, 0.99f, ACCESS_ORDER);
        this.maxCacheSize = maxCacheSize;
    }

    @Override
    protected boolean removeEldestEntry(final Map.Entry<K, V> eldest) {
        return size() > maxCacheSize;
    }

}
