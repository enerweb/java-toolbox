package za.co.enerweb.toolbox.collections;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class SessionManager {
    private final Map sessionMap = new HashMap();

    public synchronized void deleteSession(final String session_id) {
        sessionMap.remove(session_id);
    }

    public synchronized Collection deleteStaleSessions(
        final int unusedMinutes) {
        return deleteStaleSessions(unusedMinutes, 0);
    }

    /**
     * @param unusedMinutes
     * @param unusedSeconds
     * @return a log of what was deleted
     */
    public synchronized Collection deleteStaleSessions(final int unusedMinutes,
        final int unusedSeconds) {
        Collection ret = new ArrayList();
        Date cutoffTime = new Date();
        cutoffTime.setTime(cutoffTime.getTime() -
            (unusedMinutes * 60 + unusedSeconds) * 1000);
        // System.out.println("cutoffTime : " + cutoffTime);
        for (Iterator iterator = sessionMap.entrySet().iterator(); iterator
            .hasNext();) {
            Map.Entry entry = (Map.Entry) iterator.next();
            SessionObjectContainer soc =
                (SessionObjectContainer) entry.getValue();
            // System.out.println("session_id : " + entry.getKey());
            // System.out.println("LastUsedDate : " + soc.getLastUsedDate());
            if (soc.getLastUsedDate().before(cutoffTime)) {
                // System.out.println("deleting..");
                ret.add(entry.getKey());
                iterator.remove();
            }
        }
        return ret;
    }

    public Collection getAllSessionIds() {
        return sessionMap.keySet();
    }

    /**
     * @param session_id
     * @return null if the session does not exist
     */
    public SessionObjectContainer getExistingSession(final String session_id) {
        return (SessionObjectContainer) sessionMap.get(session_id);
    }

    public synchronized SessionObjectContainer getOrCreateSessionObject(
        final String session_id) {
        SessionObjectContainer soc = getExistingSession(session_id);
        if (soc == null) {
            soc = new SessionObjectContainer();
            sessionMap.put(session_id, soc);
        }
        return soc;
    }
}
