/**
 * Created on 2007/04/17
 */
package za.co.enerweb.toolbox.collections;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

/**
 * Its like a queue of elements,
 * but the oldest (first) elements will be popped of the list if there isn't
 * enough space for new ones (last).
 * Ideal way to store a history buffer or a clipboard buffer,
 * where you want to store a rolling history of objects,
 * as to keep memory usage sane.
 */
public class Buffer implements List {
    LinkedList buffer = new LinkedList();

    //
    // attributes
    //

    private int capacity;

    //
    // constructors
    //

    public Buffer(final int capacity) {
        setCapacity(capacity);
    }

    //
    // overriding of HashSet
    //

    public void add(final int index, final Object element) {
        buffer.add(index, element);
        ensureCapacity();
    }

    public boolean add(final Object element) {
        buffer.add(element);
        ensureCapacity();
        return true;
    }

    //
    // overriding of AbstractCollection
    //

    public boolean addAll(final Collection collection) {
        boolean ret = buffer.addAll(collection);
        ensureCapacity();
        return ret;
    }

    //
    // other methods
    //

    public boolean addAll(final int index, final Collection c) {
        return false;
    }

    public void clear() {
        buffer.clear();
    }

    public boolean contains(final Object o) {
        return buffer.contains(o);
    }

    public boolean containsAll(final Collection c) {
        return buffer.containsAll(c);
    }

    private void ensureCapacity() {
        for (int i = buffer.size(); i > capacity; i--) {
            /*
             * left her for possible future debugging
             * AssertLog.log("Buffer[" + buffer.size() + "/" +
             * capacity + "] removing item: " + buffer.getFirst());
             */
            buffer.removeFirst();
        }
    }

    @Override
    public boolean equals(final Object o) {
        return buffer.equals(o);
    }

    public Object get(final int index) {
        return buffer.get(index);
    }

    public int getCapacity() {
        return capacity;
    }

    /**
     * @return the oldest element to be inserted
     */
    public Object getFirst() {
        return buffer.getFirst();
    }

    /**
     * @return the latest element to be inserted
     */
    public Object getLast() {
        return buffer.getLast();
    }

    @Override
    public int hashCode() {
        return buffer.hashCode();
    }

    public int indexOf(final Object o) {
        return buffer.indexOf(o);
    }

    public boolean isEmpty() {
        return buffer.isEmpty();
    }

    public Iterator iterator() {
        return buffer.iterator();
    }

    public int lastIndexOf(final Object o) {
        return buffer.lastIndexOf(o);
    }

    public ListIterator listIterator() {
        return buffer.listIterator();
    }

    public ListIterator listIterator(final int index) {
        return buffer.listIterator(index);
    }

    public Object remove(final int index) {
        return buffer.remove(index);
    }

    public boolean remove(final Object o) {
        return buffer.remove(o);
    }

    public boolean removeAll(final Collection c) {
        return buffer.removeAll(c);
    }

    public Object removeFirst() {
        return buffer.removeFirst();
    }

    public Object removeLast() {
        return buffer.removeLast();
    }

    public boolean retainAll(final Collection c) {
        return buffer.retainAll(c);
    }

    public Object set(final int index, final Object element) {
        return buffer.set(index, element);
    }

    private void setCapacity(final int capacity) {
        if (capacity <= 0) {
            throw new IllegalArgumentException(
                "cannot set capacity to less than 1");
        }
        this.capacity = capacity;
        ensureCapacity();
    }

    public int size() {
        return buffer.size();
    }

    public List subList(final int fromIndex, final int toIndex) {
        return buffer.subList(fromIndex, toIndex);
    }

    public Object[] toArray() {
        return buffer.toArray();
    }

    public Object[] toArray(final Object[] a) {
        return buffer.toArray(a);
    }

    @Override
    public String toString() {
        return buffer.toString();
    }

}
