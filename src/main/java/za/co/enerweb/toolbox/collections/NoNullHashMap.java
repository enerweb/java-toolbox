package za.co.enerweb.toolbox.collections;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * This is a HashMap that can never contain
 * null keys or values!
 * @author Marius Kruger
 */
public class NoNullHashMap extends HashMap {

    private static final long serialVersionUID = 1L;

    public NoNullHashMap() {
    }

    public NoNullHashMap(int initialCapacity) {
        super(initialCapacity);
    }

    public NoNullHashMap(int initialCapacity, float loadFactor) {
        super(initialCapacity, loadFactor);
    }

    public NoNullHashMap(Map m) {
        super(m.size());
        // the normal HashMap(Map) constructor could add null values,
        // so we take special care not to do that.
        for (Iterator iter = m.entrySet().iterator(); iter.hasNext();) {
            Map.Entry entry = (Map.Entry) iter.next();
            put(entry.getKey(), entry.getValue());
        }
    }

    /**
     * Only add if the key and value is not null.
     * @param key
     * @param value
     * @return previous value associated with specified key, or <tt>null</tt> if
     *         there was no mapping for key. A <tt>null</tt> return can
     *         also indicate that the HashMap previously associated
     *         <tt>null</tt> with the specified key, or that the key or value
     *         was null and therefore wasn't added.
     */
    @Override
    public Object put(Object key, Object value) {
        if (key != null && value != null) {
            return super.put(key, value);
        }
        return null;
    }

}
