package za.co.enerweb.toolbox.collections;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import lombok.Getter;
import za.co.enerweb.toolbox.string.StringBuilderUtil;

/**
 * Enables you to easily construct a byte[] in chunks without the parts
 * always needing to get copied.
 * Integers are written as 4 bytes.
 */
public class ByteArrayBuilder {

    private List<Object> chunks = new ArrayList<Object>();
    @Getter
    private int size;

    public ByteArrayBuilder prepend(byte aByte) {
        chunks.add(0, aByte);
        size++;
        return this;
    }

    public ByteArrayBuilder prepend(int aInt) {
        chunks.add(0, aInt);
        size += 4;
        return this;
    }

    public ByteArrayBuilder prepend(byte[] bytes) {
        chunks.add(0, bytes);
        size += bytes.length;
        return this;
    }

    public ByteArrayBuilder append(byte aByte) {
        chunks.add(aByte);
        size++;
        return this;
    }

    public ByteArrayBuilder append(int aInt) {
        chunks.add(aInt);
        size += 4;
        return this;
    }

    public static byte[] intTo4Bytes(int aInt) {
        return ByteBuffer.allocate(4).putInt(aInt).array();
    }

    public ByteArrayBuilder append(byte[] bytes) {
        chunks.add(bytes);
        size += bytes.length;
        return this;
    }

    // public ByteArrayBuilder insert(byte[] bytes, int index) {
    // return insert(bytes, index, bytes.length);
    // }
    //
    // public ByteArrayBuilder insert(byte[] bytes, int index, int length) {
    // // find the insertion point,
    // // is it on a boundry?
    // // split the chunk if needed..
    //
    // // chunks.add(index, bytes);
    // throw new NotImplementedException();
    // }

    public byte[] build() {
        byte[] ret = new byte[size];
        int i = 0;
        for (Object o : chunks) {
            if (o instanceof byte[]) {
                byte[] chunk = (byte[]) o;
                i = writeChunk(ret, i, chunk);
            } else if (o instanceof Byte) {
                ret[i++] = (Byte) o;
            } else if (o instanceof Integer) {
                i = writeChunk(ret, i, intTo4Bytes((Integer) o));
            } else {
                throw new IllegalStateException(
                    "Only supposed to contain objects of type Byte or byte[]");
            }
        }
        if (i!=size) {
            throw new IllegalStateException("size was invalid: " + size + "!="
                + i);
        }
        return ret;
    }

    public int writeChunk(byte[] ret, int i, byte[] chunk) {
        System.arraycopy(chunk, 0, ret, i, chunk.length);
        i += chunk.length;
        return i;
    }

    public String toDebugString() {
        StringBuilderUtil ret = new StringBuilderUtil();
        ret.addLine("size: " + size);
        for (Object o : chunks) {
            if (o instanceof byte[]) {
                byte[] chunk = (byte[]) o;
                ret.addLine(Arrays.toString(chunk));
            } else if (o instanceof Byte) {
                ret.addLine("(byte)" + String.valueOf((Byte) o));
            } else if (o instanceof Integer) {
                ret.addLine("(int)" + String.valueOf((Integer) o));
            } else {
                throw new IllegalStateException(
                    "Only supposed to contain objects of type Byte or byte[]");
            }
        }
        return ret.toString();
    }
}
