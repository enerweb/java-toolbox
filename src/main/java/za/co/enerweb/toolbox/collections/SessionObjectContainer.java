package za.co.enerweb.toolbox.collections;

import java.io.Serializable;
import java.util.Date;

public class SessionObjectContainer implements Serializable {
    private static final long serialVersionUID = 1L;
    private transient Object object; // dont serialise the session object for
    // now
    private final Date creationDate = new Date();
    private Date lastUsedDate = new Date();

    public Date getCreationDate() {
        return creationDate;
    }

    public Date getLastUsedDate() {
        return lastUsedDate;
    }

    public Object getObject() {
        lastUsedDate = new Date();
        return object;
    }

    public void setObject(final Object object) {
        lastUsedDate = new Date();
        this.object = object;
    }
}
