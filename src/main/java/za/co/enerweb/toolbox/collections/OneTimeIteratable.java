package za.co.enerweb.toolbox.collections;

import java.util.Iterator;
import lombok.AllArgsConstructor;

/**
 * This is a light Iterable wrapper around an iterator.
 * That means it van only be iterated once, because there is no way
 * to get another iterator again.
 */
@AllArgsConstructor
public class OneTimeIteratable<T> implements Iterable<T> {
    private Iterator<T> iterator;

//    public OneTimeIteratable(Iterator<HomeFile> chainedIterator) {
//        throw new UnsupportedOperationException("Not supported yet.");
//    }
    @Override
    public Iterator<T> iterator() {
        if (iterator == null) {
            throw new IllegalStateException("This Iterateable can only be " +
                    "iterated one.");
        }
        Iterator<T> ret = iterator;
        iterator = null;
        return ret;
    }

}
