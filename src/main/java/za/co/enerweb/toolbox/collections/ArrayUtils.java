package za.co.enerweb.toolbox.collections;

import java.lang.reflect.Array;

public abstract class ArrayUtils {

    /**
     * Append a value to the back of an array, returning a new array.
     */
    public static <T> T[] append(Class<T> klass, T[] front, T back) {

        @SuppressWarnings("unchecked") T[] ret = (T[]) Array.newInstance(klass,
            front.length + 1);

        System.arraycopy(front, 0, ret, 0, front.length);
        ret[front.length] = back;

        return ret;
    }

    /**
     * Join 2 arrays, returning a new array.
     */
    public static <T> T[] join(Class<T> klass, T[] front, T[] back) {

        @SuppressWarnings("unchecked") T[] ret = (T[]) Array.newInstance(klass,
            front.length + back.length);

        System.arraycopy(front, 0, ret, 0, front.length);
        System.arraycopy(back, 0, ret, front.length, back.length);

        return ret;
    }

    /**
     * Convenient way to join 2 arrays, it's useful if you want to specify an
     * unknown number of front values and an array of back values.
     */
    public static <T> T[] joinBackToFront(Class<T> klass, T[] back,
        T... front) {
        return join(klass, front, back);
    }

    /**
     * maybe use Arrays.copyOfRange
     * @param klass
     * @param array
     * @param startIndex inclusive
     * @param endIndex exclusive
     * @return
     */
    public static <T> T[] subArray(Class<T> klass, T[] array, int startIndex,
        int endIndex) {

        int length = endIndex - startIndex;
        @SuppressWarnings("unchecked") T[] ret = (T[]) Array.newInstance(klass,
            length);

        System.arraycopy(array, startIndex, ret, 0, length);

        return ret;
    }
}
