/**
 * Created on 2007/04/17
 */
package za.co.enerweb.toolbox.collections;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashSet;

/**
 * @author Elardus Viljoen
 */
public class CacheSet extends LinkedHashSet {

    //
    // attributes
    //

    private static final long serialVersionUID = 1L;
    private int capacity;

    //
    // constructors
    //

    public CacheSet(int capacity) {
        super(capacity + 1, 0.99f);
        setCapacity(capacity);
    }

    //
    // overriding of HashSet
    //

    @Override
    public boolean add(Object element) {
        remove(element);
        super.add(element);

        if (isCapacityExeeded()) {
            removeEldestEntry();
        }
        return true;
    }

    //
    // overriding of AbstractCollection
    //

    @Override
    public boolean addAll(Collection collection) {
        for (Iterator it = collection.iterator(); it.hasNext();) {
            add(it.next());
        }
        return true;
    }

    //
    // other methods
    //

    public int getCapacity() {
        return capacity;
    }

    protected boolean isCapacityExeeded() {
        return size() > capacity;
    }

    protected void removeEldestEntry() {
        Object eldest = iterator().next();
        remove(eldest);
    }

    private void setCapacity(int capacity) {
        if (capacity <= 0) {
            throw new IllegalArgumentException(
                "cannot set capacity to less than 1");
        }
        this.capacity = capacity;
    }

}
