package za.co.enerweb.toolbox.collections;

import java.util.Enumeration;
import java.util.Iterator;

public class EnumerationIteratorAdaptor<T> implements Iterator<T> {

    private final Enumeration<T> enumeration;

    public EnumerationIteratorAdaptor(Enumeration<T> enumeration) {
        this.enumeration = enumeration;
    }

    @Override
    public boolean hasNext() {
        return enumeration.hasMoreElements();
    }

    @Override
    public T next() {
        return enumeration.nextElement();
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException();
    }

}
