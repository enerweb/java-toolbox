/*
 * Created on Apr 6, 2005
 */
package za.co.enerweb.toolbox.objects;

/**
 * @author Marius Kruger
 */
public class ObjectUtils {

    /**
     * return true if both are null OR they are equal.
     * @param o1
     * @param o2
     * @return
     */
    public static final boolean equals(Object o1, Object o2) {
        return o1 == null && o2 == null ||
            o1 != null && o2 != null && o1.equals(o2);
    }

    /**
     * add obj.hashcode + 29*oldHashcode
     * @param oldHashcode
     * @param obj
     * @return
     */
    public static final int hashCode(final int oldHashcode, Object obj) {
        if (obj == null) {
            return oldHashcode * 29;
        }
        return oldHashcode * 29 + obj.hashCode();
    }

    /**
     * Returns 0 if obj is null, or the hashcode if it is not null
     * @param obj
     * @return
     */
    public static final int hashCode(Object obj) {
        if (obj == null) {
            return 0;
        }
        return obj.hashCode();
    }

    public static final int hashCode(Object[] objs) {
        int h = 0;
        for (Object obj : objs) {
            h = hashCode(h, obj);
        }
        return h;
    }

}
