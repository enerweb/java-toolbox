package za.co.enerweb.toolbox.enums;

/**
 * @author Rudolf Visagie
 */
public class EnumInterfaceItem {

    //
    // attributes
    //

    private String name;
    private String caption;

    //
    // constructors
    //

    public EnumInterfaceItem() {
    }

    public EnumInterfaceItem(String name, String caption) {
        super();
        this.name = name;
        this.caption = caption;
    }

    //
    // overriding of Object
    //

    public String getCaption() {
        return caption;
    }

    //
    // accessors & mutators
    //

    public String getName() {
        return name;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return caption;
    }

}
