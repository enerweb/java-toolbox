package za.co.enerweb.toolbox.enums;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.Collection;
import java.util.LinkedList;

/**
 * Super cool ultimate Enum class for managing properties with a fixed set of
 * possible values. Mimics the c++ enum. The special string value NULL is
 * allocated if currentItem is not assigned (currentItem == null).
 * Raison d'etre: It works with Java 1.4 and Java 5.
 * @author Marius Kruger
 */
public class Enum implements EnumInterface, Serializable, Cloneable {

    //
    // constants
    //

    public static class EnumItem implements Serializable {

        //
        // attributes
        //

        private static final long serialVersionUID = 1L;
        /* package */String humanReadable;

        //
        // constructors
        //

        public EnumItem() {
        }

        public EnumItem(String humanReadable) {
            this.humanReadable = humanReadable;
        }

    }

    private static final long serialVersionUID = 1L;

    //
    // attributes
    //

    public static final String NULL;
    /* only needed for xml rpc */
    private String name;

    //
    // init blocks
    //

    protected transient EnumItem currentItem;

    //
    // constructors
    //

    static {
        NULL = "NULL";
    }

    public Enum() {
    }

    public Enum(EnumItem item) {
        setItem(item);
    }

    //
    // implementation of EnumInterface
    //

    public Enum(String name) throws EnumException {
        setName(name);
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        try {
            Enum instance = getClass().newInstance();
            instance.setItem(currentItem);
            return instance;
        } catch (Exception ex) {
            ex.printStackTrace();
            String msg = "Unable to clone object: " + ex.getMessage();
            throw new CloneNotSupportedException(msg);
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof EnumItem) {
            EnumItem item = (EnumItem) obj;
            return item == currentItem;
        } else if (obj instanceof Enum) {
            Enum curEnum = (Enum) obj;
            return curEnum.currentItem == currentItem;
        } else if (obj instanceof String) {
            String enumItemName = (String) obj;
            try {
                return getName().equals(enumItemName);
            } catch (EnumException ex) { /* ignore */
            }
        }
        return false;
    }

    protected Enum.EnumItem getCurrentItem() {
        return currentItem;
    }

    //
    // overriding of Object
    //

    public EnumInterfaceItem[] getEnumItems() {
        EnumInterface[] enums = getItems();
        EnumInterfaceItem[] items = new EnumInterfaceItem[enums.length];
        for (int i = 0; i < enums.length; i++) {
            EnumInterfaceItem item = new EnumInterfaceItem();
            try {
                item.setName(enums[i].getName());
                item.setCaption(enums[i].getHumanReadable());
            } catch (EnumException ex) {
                ex.printStackTrace();
            }
            items[i] = item;
        }
        return items;
    }

    public String getHumanReadable() throws EnumException {
        if (isNull()) {
            return NULL;
        }

        try {
            Field fields[] = getClass().getFields();
            for (Field field2 : fields) {
                Object field = field2.get(null);
                if (field instanceof EnumItem) {
                    EnumItem item = (EnumItem) field;
                    if (item == currentItem) {
                        if (item.humanReadable != null) {
                            return item.humanReadable;
                        } else {
                            return field2.getName();
                        }
                    }
                }
            }
        } catch (IllegalAccessException ex) { /* ignore */
        }

        throw new EnumException("This Enum is invalid.");
    }

    public Enum[] getItems() {
        return (Enum[]) getItemsCollection().toArray(new Enum[0]);
    }

    //
    // accessors & mutators
    //

    /**
     * Returns an aray of the actual Enums each set to a different value.
     */
    public LinkedList getItemsCollection() {
        LinkedList items = new LinkedList();
        try {
            Field fields[] = getClass().getFields();
            for (Field field2 : fields) {
                Object field = field2.get(null);
                if (field instanceof EnumItem) {
                    try {
                        Enum tmp = getClass().newInstance();
                        tmp.setItem((EnumItem) field);
                        items.add(tmp);
                    } catch (Exception ex) { /* ignore */
                    }
                }
            }
        } catch (Exception ex) { /* ignore */
        }
        return items;
    }

    //
    // other methods
    //

    public String getName() throws EnumException {
        if (currentItem == null) {
            return NULL;
        }

        try {
            Field fields[] = getClass().getFields();
            for (Field field2 : fields) {
                Object field = field2.get(null);
                if (field instanceof EnumItem) {
                    EnumItem item = (EnumItem) field;
                    if (item == currentItem) {
                        return field2.getName();
                    }
                }
            }
        } catch (IllegalAccessException ex) { /* ignore */
        }

        throw new EnumException("This Enum is invalid.");
    }

    public String[] getNames() {
        return (String[]) getNamesCollection().toArray(new String[0]);
    }

    public Collection getNamesCollection() {
        LinkedList items = new LinkedList();
        try {
            Field fields[] = getClass().getFields();
            for (Field field2 : fields) {
                Object field = field2.get(null);
                if (field instanceof EnumItem) {
                    items.add(field2.getName());
                }
            }
        } catch (Exception ex) { /* ignore */
        }
        return items;
    }

    public boolean isNull() {
        return currentItem == null;
    }

    // object serialized as name of currentItem
    private void readObject(ObjectInputStream stream)
        throws IOException, ClassNotFoundException {
        byte[] bytes = new byte[stream.available()];
        stream.read(bytes);
        try {
            setName(String.valueOf(bytes));
        } catch (EnumException ex) {
            try {
                currentItem =
                    ((Enum) getClass().newInstance()).getCurrentItem();
            } catch (Exception ex2) {
                currentItem = null;
            }
        }
    }

    public void setByName(String name) throws InvalidEnumName {
        try {
            this.name = name;
            setName(name);
        } catch (EnumException ex) {
            throw new InvalidEnumName(ex.getMessage(), ex.getCause());
        }
    }

    protected void setItem(EnumItem item) {
        currentItem = item;
        try {
            name = getName();
        } catch (EnumException ex) {
            name = NULL; // something went wrong
        }
    }

    /**
     * Set this enum's value according to the string passed.
     */
    public void setName(String name) throws EnumException {
        currentItem = null;
        if (!name.equals(NULL)) {
            try {
                Field fields[] = getClass().getFields();
                for (Field field2 : fields) {
                    Object field = field2.get(null);
                    if (field instanceof EnumItem
                        && field2.getName().equals(name)) {
                        setItem((EnumItem) field);
                        return;
                    }
                }
            } catch (IllegalAccessException ex) { /* ignore */
            }

            String msg = "The name \"" + name + "\" is not a valid EnumItem.";
            throw new EnumException(msg);
        }
        this.name = name;
    }

    @Override
    public String toString() {
        try {
            return getHumanReadable();
        } catch (EnumException ex) {
            return ex.getMessage(); // verify this as correct return value
        }
    }

    //
    // nested classes
    //

    // object serialized as name of currentItem
    private void writeObject(ObjectOutputStream stream) throws IOException {
        try {
            stream.write(getName().getBytes());
        } catch (EnumException ex) {
            stream.write(NULL.getBytes());
        }
        stream.flush();
    }

}
