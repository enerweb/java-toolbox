package za.co.enerweb.toolbox.enums;

/**
 * @author Marius Kruger
 */
public class EnumException extends Exception {

    //
    // constructors
    //

    private static final long serialVersionUID = 1L;

    public EnumException() {
        super();
    }

    public EnumException(String message) {
        super(message);
    }

    public EnumException(String message, Throwable cause) {
        super(message, cause);
    }

    public EnumException(Throwable cause) {
        super(cause);
    }

}
