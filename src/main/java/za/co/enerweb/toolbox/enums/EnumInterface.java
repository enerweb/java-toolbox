package za.co.enerweb.toolbox.enums;

/**
 * @author Rudolf Visagie
 */
public interface EnumInterface {

    public EnumInterfaceItem[] getEnumItems();

    public String getHumanReadable() throws EnumException;

    public String getName() throws EnumException;

    public void setByName(String name) throws InvalidEnumName;

}
