package za.co.enerweb.toolbox.enums;

/**
 * @author Rudolf Visagie
 */
public class InvalidEnumName extends Exception {

    //
    // constructors
    //

    private static final long serialVersionUID = 1L;

    public InvalidEnumName(String message) {
        super(message);
    }

    public InvalidEnumName(String message, Throwable cause) {
        super(message, cause);
    }

}
