package za.co.enerweb.toolbox.reflection;

import java.lang.reflect.Array;
import java.lang.reflect.Constructor;

import org.apache.commons.lang.StringUtils;

public class ClassUtils {

    /**
     * Returns the associated wrapper class when specifying a primitive class.
     */
    public static Class getWrapperClass(Class cls) {
        if (!cls.isPrimitive()) {
            return cls;
        }

        if (cls == int.class) {
            return Integer.class;
        } else if (cls == double.class) {
            return Double.class;
        } else if (cls == boolean.class) {
            return Boolean.class;
        } else if (cls == long.class) {
            return Long.class;
        } else if (cls == float.class) {
            return Float.class;
        } else if (cls == short.class) {
            return Short.class;
        } else if (cls == byte.class) {
            return Byte.class;
        } else if (cls == char.class) {
            return Character.class;
        }

        return null;
    }

    /**
     * Returns the associated wrapper class when specifying a primitive class
     * name.
     */
    public static Class getWrapperClass(String className) {
        if (className == null) {
            String msg = "may not specify null class name";
            throw new IllegalArgumentException(msg);
        } else if (className.length() == 0) {
            String msg =
                "may not specify a class name containing no characters";
            throw new IllegalArgumentException(msg);
        }

        if ("int".equals(className)) {
            return Integer.class;
        } else if ("double".equals(className)) {
            return Double.class;
        } else if ("boolean".equals(className)) {
            return Boolean.class;
        } else if ("long".equals(className)) {
            return Long.class;
        } else if ("float".equals(className)) {
            return Float.class;
        } else if ("short".equals(className)) {
            return Short.class;
        } else if ("byte".equals(className)) {
            return Byte.class;
        } else if ("char".equals(className)) {
            return Character.class;
        }

        return null;
    }

    /**
     * Returns an instance of the specified class.
     * <ul>
     * <li>convert primitive to wrapper class</li>
     * <li>returns an array instance if the class is an array</li>
     * <li>try to return an Object created by the default constructor</li>
     * <li>try to return an Object created by a constructor with a single String
     * parameter &quot;0&quot;</li>
     * <li>try to return an Object created by a constructor with a single char
     * parameter '0'</li>
     * <li>try to return an Object created by a constructor with a single long
     * parameter 0</li>
     * <li>if this point is reached, return null</li>
     * </ul>
     */
    public static Object instantiate(Class cls) {
        if (cls == null) {
            String msg = "may not specify null Class parameter";
            throw new IllegalArgumentException(msg);
        }

        if (cls.isPrimitive()) {
            cls = getWrapperClass(cls);
        }

        if (cls.isArray()) {
            cls = cls.getComponentType();
            return Array.newInstance(cls, 0);
        }

        try {
            Constructor constr = cls.getConstructor(new Class[0]);
            return constr.newInstance(new Object[0]);
        } catch (Exception ex) { /* ignore */
        }

        try {
            Constructor constr = cls.getConstructor(new Class[] {String.class});
            return constr.newInstance(new Object[] {"0"});
        } catch (Exception ex) { /* ignore */
        }

        try {
            Constructor constr =
                cls.getConstructor(new Class[] {Character.TYPE});
            return constr.newInstance(new Object[] {new Character('0')});
        } catch (Exception ex) { /* ignore */
        }

        try {
            Constructor constr = cls.getConstructor(new Class[] {Long.TYPE});
            return constr.newInstance(new Object[] {new Long(0)});
        } catch (Exception ex) { /* ignore */
        }

        String msg = "unable to instantate class \"" + cls + "\"";
        System.err.println(msg);
        return null;
    }

    @SuppressWarnings("unchecked")
    public static boolean hasMethod(Class klass, String methodName) {
        try {
            klass.getMethod(methodName);
            return true;
        } catch (SecurityException e) {
        } catch (NoSuchMethodException e) {
        }
        return false;
    }

    @SuppressWarnings("rawtypes")
    public static boolean hasGetter(Class klass, String propertyName) {
        return hasMethod(klass, "get" + StringUtils.capitalize(propertyName));
    }
}
