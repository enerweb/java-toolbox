package za.co.enerweb.toolbox.reflection;

public class NotAnObjectException extends Exception {
    private static final long serialVersionUID = 1L;

    public NotAnObjectException(final String className) {
        super("'" + className + "' does not represent an object.");
    }

}
