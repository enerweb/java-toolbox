package za.co.enerweb.toolbox.reflection;

public class InvalidPropertyException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public InvalidPropertyException() {
    }

    public InvalidPropertyException(String message) {
        super(message);
    }

    public InvalidPropertyException(Throwable cause) {
        super(cause);
    }

    public InvalidPropertyException(String message, Throwable cause) {
        super(message, cause);
    }

}
