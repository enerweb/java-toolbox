/*
 * 2006-08-17
 */
package za.co.enerweb.toolbox.reflection;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author Marius Kruger
 */
public class ClassHierarchyUtils {

    /**
     * Does not take intefaces in to account.
     * @param a
     * @param b
     * @return
     */
    public static Class getClosestCommonSuperClass(Class a, Class b) {
        if (a.isAssignableFrom(b)) {
            return a;
        }
        if (b.isAssignableFrom(a)) {
            return b;
        }
        List a_supers = getSuperClasses(a);
        List b_supers = getSuperClasses(b);
        for (Iterator iter = a_supers.iterator(); iter.hasNext();) {
            Class a_super = (Class) iter.next();
            for (Iterator iterator = b_supers.iterator(); iterator.hasNext();) {
                Class b_super = (Class) iterator.next();
                if (a_super.isAssignableFrom(b_super)) {
                    return a_super;
                }
                if (b_super.isAssignableFrom(a_super)) {
                    return b_super;
                }
            }
        }
        return Object.class; // should not get here
    }

    /**
     * @param clazz
     * @return a list of this class's super classes
     */
    public static List getSuperClasses(Class clazz) {
        ArrayList ret = new ArrayList();
        while (clazz != null) {
            ret.add(clazz);
            clazz = clazz.getSuperclass();
        }
        return ret;
    }
}
