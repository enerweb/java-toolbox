/*
 * 2006-09-20
 */
package za.co.enerweb.toolbox.reflection;

import java.lang.reflect.Array;
import java.util.HashMap;
import java.util.Map;

public class ClassNameUtils {

    private static final Map<String, Class<?>> PRIMITIVE_TYPES;

    static {
        PRIMITIVE_TYPES = new HashMap<String, Class<?>>(8);
        PRIMITIVE_TYPES.put("boolean", Boolean.TYPE);
        PRIMITIVE_TYPES.put("byte", Byte.TYPE);
        PRIMITIVE_TYPES.put("char", Character.TYPE);
        PRIMITIVE_TYPES.put("double", Double.TYPE);
        PRIMITIVE_TYPES.put("float", Float.TYPE);
        PRIMITIVE_TYPES.put("int", Integer.TYPE);
        PRIMITIVE_TYPES.put("long", Long.TYPE);
        PRIMITIVE_TYPES.put("short", Short.TYPE);
    }

    public static String deriveArrayType(final String baseType,
        final int arrayDimension) {
        if (arrayDimension > 0) {
            String arrayType = "L" + baseType + ";";
            for (int i = 0; i < arrayDimension; i++) {
                arrayType = "[" + arrayType;
            }
            return arrayType;
        }
        return baseType;
    }

    /**
     * @param className the base type of the array
     *        (or className if its not an array)
     * @return
     */
    public static String getArrayBaseComponentType(final String className) {
        String ret = className.replaceFirst("^\\[+", "");

        if (ret.length() == 1) {
            switch (ret.charAt(0)) {
            case 'Z':
                return "boolean";
            case 'B':
                return "byte";
            case 'C':
                return "char";
            case 'D':
                return "double";
            case 'F':
                return "float";
            case 'I':
                return "int";
            case 'J':
                return "long";
            case 'S':
                return "short";
            }
        }

        // remove L;
        if (ret.length() >= 2 && ret.charAt(0) == 'L'
            && ret.charAt(ret.length() - 1) == ';') {
            return ret.substring(1, ret.length() - 1);
        }
        // Matcher m = Pattern.compile("L(.+);").matcher(ret);
        return ret;
    }

    /**
     * @param className
     * @return the number of dimensions of the array represented by className,
     *         and zero, if it isn't an array.
     */
    public static int getArrayDimensions(final String className) {
        int ret = 0;
        for (; ret < className.length()
            && className.charAt(ret) == '['; ret++) {
            ;
        }
        return ret;
    }

    public static Class<?> getClass(final String className)
        throws ClassNotFoundException {
        if (className == null) {
            String msg = "may not specify null name parameter";
            throw new IllegalArgumentException(msg);
        } else if (className.trim().length() == 0) {
            String msg =
                "may not specify name parameter containing zero characters";
            throw new IllegalArgumentException(msg);
        }

        if (PRIMITIVE_TYPES.containsKey(className)) {
            return PRIMITIVE_TYPES.get(className);
        }

        if (isArray(className)) {
            return Array.newInstance(
                getClass(getArrayBaseComponentType(className)),
                new int[getArrayDimensions(className)]).getClass();
        }

        // make sure we use the calling thread's class loader and not the
        // classloader of this class!
        return Class.forName(className, true,
            Thread.currentThread().getContextClassLoader());
    }

    /**
     * Changes a classnames of arrays to a more conventional format.
     */
    public static String getHumanReadableClassName(final String className) {
        if (isArray(className)) {
            StringBuffer buffer =
                new StringBuffer(getArrayBaseComponentType(className));
            int dimensions = getArrayDimensions(className);
            for (int i = 0; i < dimensions; i++) {
                buffer.append("[]");
            }
            return buffer.toString();
        }
        return className;
    }

    /**
     * get the class and ClassUtils.instantiate.
     */
    public static Object instantiate(final String className)
        throws ClassNotFoundException {
        Class cls = getClass(className);
        return ClassUtils.instantiate(cls);
    }

    /**
     * Checks if className represents an array.
     * @return true if className represents an array
     */
    public static boolean isArray(final String className) {
        return className.startsWith("[");
    }

    /**
     * Checks if className represents an object (including primitive arrays).
     * @param className
     * @return true if className represents an object)
     */
    public static boolean isObject(final String className) {
        // check if its an array
        return isArray(className)
            // check if its a class (assumes its in a package)
            || className.indexOf(".") >= 0;
    }

    /**
     * Checks if className represents an object (including primitive arrays).
     * @throws NotAnObjectException if className does not represent an Object
     */
    public static void validateObject(final String className)
        throws NotAnObjectException {
        if (!isObject(className)) {
            throw new NotAnObjectException(className);
        }
    }

}
