package za.co.enerweb.toolbox.datetime;

import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public interface DateFormats {
    public static final String DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
    DateTimeFormatter DATE_TIME =
        DateTimeFormat.forPattern(DATE_TIME_FORMAT);
    DateTimeFormatter DATE =
        DateTimeFormat.forPattern("yyyy-MM-dd");
    DateTimeFormatter TIME =
        DateTimeFormat.forPattern("HH:mm:ss");
    DateTimeFormatter TIME_WITH_MILLIS =
        DateTimeFormat.forPattern("HH:mm:ss.SSS");
}
