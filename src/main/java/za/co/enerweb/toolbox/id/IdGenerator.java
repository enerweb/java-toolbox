package za.co.enerweb.toolbox.id;


public class IdGenerator {
    private static long currentTimeMillis = System.currentTimeMillis();

    private static synchronized long get() {
        return currentTimeMillis++;
    }

    /**
     * It is shorter but has a tiny possibility of getting duplicates
     * if generated on different systems.
     * @return
     */
    public static String getLocallyUniqueId() {
        return Long.toHexString(get());
    }

    /**
     * get universally unique id
     * @return
     */
    public static String getUuid() {
        return java.util.UUID.randomUUID().toString();
    }
}
