package za.co.enerweb.toolbox.timing;

import org.joda.time.DateTime;
import org.joda.time.Duration;

public class Stopwatch {
    protected DateTime t0 = new DateTime();
    protected DateTime tx = new DateTime();

    /**
   *
   */
    public Stopwatch() {
        super();

    }

    public String getDelta() {
        return getDeltaDuration().toString();
    }

    public Duration getDeltaDuration() {
        DateTime new_tx = new DateTime();
        Duration ret = new Duration(tx, new_tx);
        tx = new_tx;
        return ret;
    }

    public long getDeltaMilliSeconds() {
        return getDeltaDuration().getMillis();
    }

    public Duration getDuration() {
        return new Duration(t0, new DateTime());
    }

    public String getTotal() {
        return getDuration().toString();
    }

    public long getTotalMilliSeconds() {
        return getDuration().getMillis();
    }

    public void printDelta() {
        System.out.println(getDelta());
    }

    public void printDelta(String prefix) {
        System.out.println(prefix + " : " + getDelta());
    }

    public void printTotal() {
        System.out.println(getTotal());
    }

    public void printTotal(String prefix) {
        System.out.println(prefix + " : " + getTotal());
    }

    public void reset() {
        t0 = tx = new DateTime();
    }

    /**
     * reduces total by current delta
     * @return
     */
    public Duration revertTotal() {
        Duration deltaDuration = getDeltaDuration();
        t0.plus(deltaDuration);
        return deltaDuration;
    }
}
