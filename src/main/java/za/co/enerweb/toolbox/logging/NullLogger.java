package za.co.enerweb.toolbox.logging;

import java.util.logging.LogRecord;
import java.util.logging.Logger;

/**
 * Implements a logger which just ignores everything.
 * You could ever need one instance thus the singleton feel.
 * @author Marius Kruger
 */
public class NullLogger extends Logger {
    private static NullLogger staticInstance = new NullLogger();

    public static Logger getInstance() {
        return staticInstance;
    }

    private NullLogger() {
        super("za.co.enerweb.toolbox.logging.NullLogger", null);
    }

    @Override
    public void log(LogRecord record) {
        // I can't hear you!
    }
}
