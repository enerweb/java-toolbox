package za.co.enerweb.toolbox.logging;

import java.text.DateFormat;
import java.util.Date;

/**
 * Evil misuse of assert to log specific things only when runned when asserts
 * are eneabled. (this is normallay enabled by passing -ea to the JVM.)
 * @author Marius Kruger
 */
public class AssertLog {

    public final static boolean assertsEnabled;

    static {
        boolean ea = false;
        assert ea = true;
        assertsEnabled = ea;
    }

    /**
     * Only log if aserts are enabled!
     * @param message
     */
    public static void log(String message) {
        if (assertsEnabled) {
            // say where the log is from
            StackTraceElement[] stack = new Exception().getStackTrace();
            if (stack.length > 1) {
                System.out.println("[AssertLog] " +
                    DateFormat.getDateTimeInstance().format(new Date())
                    + " " + stack[1] + " :");
            }
            System.out.println(message);
        }
    }

    public static void main(String[] args) {
        log("AssertLog is enabled at the moment!");
    }
}
