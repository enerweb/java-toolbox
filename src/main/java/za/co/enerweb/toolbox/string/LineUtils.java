package za.co.enerweb.toolbox.string;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.NoSuchElementException;

import za.co.enerweb.toolbox.collections.EnumerationIteratorAdaptor;
import za.co.enerweb.toolbox.collections.OneTimeIteratable;

/**
 * This class takes a string and convert it to a list of strings, each
 * representing a line and its hash code
 */
public class LineUtils {

    public static class LineHashPair {
        String line;
        int hash;

        LineHashPair() {
        }

        /**
         * @param line
         */
        public LineHashPair(String line) {
            this.line = line;
            this.hash = line.hashCode();
        }

        /**
         * @param line
         * @param hash
         */
        public LineHashPair(String line, int hash) {
            this.line = line;
            this.hash = hash;
        }

        @Override
        public boolean equals(Object obj) {
            if (!(obj instanceof LineHashPair)) {
                return false;
            }
            LineHashPair lhp = (LineHashPair) obj;
            return hash == lhp.hash && line.equals(lhp.line);
        }

        @Override
        public int hashCode() {
            return hash;
        }

        @Override
        public String toString() {
            return line;
        }
    }

    public static String addLineNumbers(String str) {
        return addLineNumbers(str, 3);
    }

    public static String addLineNumbers(String str, int lineNumberWidth) {
        StringBuffer ret = new StringBuffer();
        Enumeration<String> enumeration = LineUtils.getLineEnumeration(str);
        int lineNumber = 1;
        while (enumeration.hasMoreElements()) {
            String line = enumeration.nextElement();
            ret.append(StringUtils.leftJustifyString(
                String.valueOf(lineNumber), lineNumberWidth)
                + ":" + line + "\r\n");
            lineNumber++;
        }
        return ret.toString();
    }

    /**
     * Enumerates lines, while returning empty lines.
     * @param str
     * @return
     */
    public static Enumeration<String> getLineEnumeration(String str) {
        final byte[] bytes = str.getBytes();
        return new Enumeration<String>() {
            int idx = 0;

            public boolean hasMoreElements() {
                if (idx == 0 && bytes.length > 0
                    && (bytes[0] == '\r' || bytes[0] == '\n')) {
                    return true;
                }
                return idx < bytes.length;
            }

            public String nextElement() {
                if (!hasMoreElements()) {
                    throw new NoSuchElementException("No more elements");
                }
                if (idx == 0 && bytes.length > 0
                    && (bytes[0] == '\r' || bytes[0] == '\n')) {
                    if (idx + 1 < bytes.length) {
                        if (bytes[idx] == '\r' && bytes[idx + 1] == '\n') {
                            idx++;// read extra character
                        }
                    }
                    idx++;
                    return "";
                }

                for (int i = idx; i < bytes.length; i++) {
                    if (bytes[i] == '\r' || bytes[i] == '\n') {
                        String ret = new String(bytes, idx, i - idx);

                        if (i + 1 < bytes.length) {
                            if (bytes[i] == '\r' && bytes[i + 1] == '\n') {
                                i++;// read extra character
                            }
                        }
                        idx = i + 1;
                        return ret;
                    }
                }
                String ret = new String(bytes, idx, bytes.length - idx);
                idx = bytes.length;
                return ret;
            }
        };
    }

    public static Iterator<String> getLineIterator(String str) {
        return new EnumerationIteratorAdaptor<String>(
            getLineEnumeration(str));
    }

    /**
     * the returned iterable can only be iterated once
     */
    public static Iterable<String> getLineIterable(String str) {
        return new OneTimeIteratable<String>(getLineIterator(str));
    }

    /**
   *
   */
    public static LineHashPair[] getLines(String str) {
        Enumeration<String> enumeration = getLineEnumeration(str);
        ArrayList<LineHashPair> ret = new ArrayList<LineHashPair>();
        while (enumeration.hasMoreElements()) {
            ret.add(new LineHashPair(enumeration.nextElement()));
        }
        return ret.toArray(new LineHashPair[ret.size()]);
    }

}
