package za.co.enerweb.toolbox.string;

public class StringMatrix {

    public static void printStringMatrix(String[][] matrix) {
        for (String[] element : matrix) {
            for (int col = 0; col < element.length; col++) {
                if (col != 0) {
                    System.out.print(", ");
                }
                System.out.print(element[col]);
            }
            System.out.println();
        }
    }
}
