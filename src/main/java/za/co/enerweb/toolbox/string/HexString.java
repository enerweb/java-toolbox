package za.co.enerweb.toolbox.string;

public class HexString {

    public static String bytes2HexString(final byte[] bytes) {
        StringBuffer sb = new StringBuffer(bytes.length * 2);
        for (byte b : bytes) {
            sb.append(convertDigit((b >> 4)));
            sb.append(convertDigit((b & 0x0f)));
        }
        return sb.toString();
    }

    /**
     * [Private] Convert the specified value (0 .. 15) to the corresponding
     * hexadecimal digit.
     * @param value Value to be converted
     */
    private static char convertDigit(int value) {
        value &= 0x0f;
        if (value >= 10) {
            return (char) (value - 10 + 'a');
        } else {
            return (char) (value + '0');
        }
    }

}
