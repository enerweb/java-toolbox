package za.co.enerweb.toolbox.string;

import java.util.List;

/**
 * Simple CSV builder, mainly needed for gwt usage.
 */
public class CsvBuilder {
    private StringBuilder sb = new StringBuilder();
    boolean atStartOfLine = true;

    public CsvBuilder add(String val) {
        addComma();
        sb.append('"');
        sb.append(val);
        sb.append('"');
        return this;
    }

    public CsvBuilder addUnquoted(String val) {
        addComma();
        sb.append(val);
        return this;
    }

    public CsvBuilder add(Double val) {
        addComma();
        sb.append(val);
        return this;
    }

    public CsvBuilder add(double val) {
        addComma();
        sb.append(val);
        return this;
    }

    public CsvBuilder add(double[] values) {
        for (double d : values) {
            add(d);
        }
        return this;
    }

    public CsvBuilder add(Integer val) {
        addComma();
        sb.append(val);
        return this;
    }

    public CsvBuilder add(int val) {
        addComma();
        sb.append(val);
        return this;
    }

    public CsvBuilder add(Boolean val) {
        addComma();
        sb.append(val);
        return this;
    }

    public CsvBuilder add(boolean val) {
        addComma();
        sb.append(val);
        return this;
    }

    public CsvBuilder add() {
        addComma();
        return this;
    }

    private void addComma() {
        if (atStartOfLine) {
            atStartOfLine = false;
        } else {
            sb.append(',');
        }
    }

    public CsvBuilder newline() {
        sb.append('\n');
        atStartOfLine = true;
        return this;
    }

    public String toString() {
        return sb.toString();
    }

    public CsvBuilder addCaptions(List<String> captions) {
        return addCaptions(-1, null, captions);
    }

    public CsvBuilder addCaptions(int indexColumn, String indexCaption,
        List<String> captions) {
        for (int i = 0; i <= captions.size(); i++) {
            if (indexColumn == i) {
                add(indexCaption);
            }
            if (i < captions.size()) {
                add(captions.get(i));
            }
        }
        newline();
        return this;
    }

    public CsvBuilder addDataXY(double[][] values) {
        return addDataXY(0, values);
    }

    public CsvBuilder addDataXY(int startRow,
        double[][] values) {
        return addDataXY(-1, startRow, values);
    }

    /**
     * @param indexColumn if > 0 add the y index to every row
     * @param values[x][y]
     */
    public CsvBuilder addDataXY(int indexColumn, int startRow,
        double[][] values) {
        int ylength = 0;
        for (int x = 0; x < values.length; x++) {
            if (values[x].length > ylength) {
                ylength = values[x].length;
            }
        }
        for (int y = startRow; y < ylength; y++) {
            for (int x = 0; x <= values.length; x++) {
                if (indexColumn == x) {
                    add(y);
                }
                if (x < values.length && values[x].length >= y) {
                    add(values[x][y]);
                }
            }
            newline();
        }
        return this;
    }
}
