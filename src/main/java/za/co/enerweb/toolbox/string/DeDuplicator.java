package za.co.enerweb.toolbox.string;

import static java.lang.String.format;

import java.io.Serializable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import lombok.Data;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * Utility class that checks for duplicate values and can then try alternative
 * names with a trailing number until a successful unique value is found. eg.
 * will search through "value 1" to "value 4" if values up to "value 3" is
 * already taken. Example:
 * <pre>
 * uniqueName = new DeDuplicator() {
 *     public boolean isValueTaken(String value) {
 *         return isNameTaken(value);
 *     }
 * }.deDupe(name);
 * </pre>
 */
// XXX: cache compile patterns (with soft/weak references) - create a
// PatternCache util!
@Setter
@Accessors(fluent = true)
public abstract class DeDuplicator implements Serializable {

    private static final long serialVersionUID = 1L;

    private int maxTries = 100;
    private String seperator = " ";
    private String combineValueFormat = null;
    private String splitValueRegex = null;

    /**
     * Override this if you need a more complicated format
     *
     * @return
     */
    public String getCombineValueFormat() {
        if (combineValueFormat == null) {
            combineValueFormat = "%1$s" + seperator + "%2$s";
        }
        return combineValueFormat;
    }

    /**
     * Override this if you need a more complicated format
     *
     * @return
     */
    public String getSplitValueRegex() {
        if (splitValueRegex == null) {
            splitValueRegex = "(.*)" + seperator + "(\\d+)";
        }
        return splitValueRegex;
    }

    public String deDupe(String orig) {

        String value = orig;
        if (isValueTaken(value)) {
            ValueComponents vc = splitValue(orig);
            int index = vc.index;
            int tries = 0;
            do {
                if (tries >= maxTries) {
                    throw new RuntimeException(format(
                        "Could not find a unique value for %s in %s retries.",
                        orig, tries));
                }
                tries++;
                value = combineValue(vc, ++index);
            } while (isValueTaken(value));
        }
        return value;
    }

    private String combineValue(ValueComponents vc, int index) {
        return String.format(getCombineValueFormat(), vc.base, index);
    }

    /**
     * @return base_name, number
     */
    public ValueComponents splitValue(String value) {
        ValueComponents ret = new ValueComponents();
        Matcher m = Pattern.compile(getSplitValueRegex()).matcher(value);
        // assumes group 1 is the base and group 2 is the index
        // override this method if you need something else..
        if (m.matches()) {
            ret.base = m.group(1);
            ret.index = Integer.parseInt(m.group(2));
        } else {
            ret.base = value;
            ret.index = 0;
        }
        return ret;
    }

    public abstract boolean isValueTaken(String value);

    @Data
    public class ValueComponents {
        private String base;
        private int index;
    }
}
