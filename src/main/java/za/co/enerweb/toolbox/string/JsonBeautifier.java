package za.co.enerweb.toolbox.string;

public class JsonBeautifier {
    private String indentation = "  ";

    // vvv used during beautify
    private boolean doublequote;
    private boolean backslash;
    private int openBraces;

     private StringBuilder sb;

    // ^^^

    public String beautify(String json) {
        doublequote = false;
        backslash = false;
        openBraces = 0;
         sb = new StringBuilder();

         for (int i = 0; i < json.length(); i++) {
         char c = json.charAt(i);
            if (c == '{') {
                openBraces++;
                sb.append(c);
                newline();
                indent();
            } else if (c == '}') {
                openBraces--;
                newline();
                indent();
                sb.append(c);
            } else if (c == ',' && !doublequote) {
                sb.append(c);
                newline();
                indent();
            } else if (c == '\\') {
                backslash = true;
                sb.append(c);
            } else if (c == '"' && !backslash) {
                doublequote = !doublequote;
                sb.append(c);
            } else {
         sb.append(c);
                backslash = false;
            }
        }
         return sb.toString();
    }

    private void newline() {
        sb.append("\n");
    }

    private void indent() {
        for (int j = 0; j < openBraces; j++) {
            sb.append(indentation);
        }
    }
}
