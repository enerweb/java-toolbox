package za.co.enerweb.toolbox.string;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.StringTokenizer;

/**
 * Utility class for methods often required.
 * TODO improve method names
 * TODO needs unit tests!
 * @author marius
 */
public class StringUtils {
    static int HORIZONTAL_LINE_WIDTH = 80;

    /**
     * @deprecated use {@link StringBuilderUtil}
     */
    @Deprecated
    public static void append(final StringBuffer sb, final String txt,
        final int tabs) {
        sb.append(repeatString("\t", tabs));
        sb.append(txt);
    }

    /**
     * Appends the message and the stack trace to the string buffer.
     * @param sb
     * @param e
     * @deprecated use {@link StringBuilderUtil}
     */
    @Deprecated
    public static void append(final StringBuffer sb, final Throwable e) {
        appendLine(sb, e.getMessage());
        appendLine(sb, getStackTraceStringFromThrowable(e));
    }

    /**
     * @deprecated use {@link StringBuilderUtil}
     */
    @Deprecated
    public static void appendLine(final StringBuffer sb, final String line) {
        sb.append(line);
        sb.append("\n");
    }

    /**
     * @deprecated use {@link StringBuilderUtil}
     */
    @Deprecated
    public static void appendLine(final StringBuffer sb, final String line,
        final int tabs) {
        sb.append(repeatString("\t", tabs));
        sb.append(line);
        sb.append("\n");
    }

    /**
     * appends text with trailing space
     * @param sb
     * @param str
     * @deprecated use {@link StringBuilderUtil}
     */
    @Deprecated
    public static void appendWord(final StringBuffer sb, final String str) {
        sb.append(str);
        sb.append(" ");
    }

    /**
     * appends both strings and then a trailing space
     * @param sb
     * @param str
     * @param punctuation
     * @deprecated use {@link StringBuilderUtil}
     */
    @Deprecated
    public static void appendWord(final StringBuffer sb, final String str,
        final String punctuation) {
        sb.append(str);
        sb.append(punctuation);
        sb.append(" ");
    }

    /**
     * abcDef => abc_Def
     * abc1Def => abc1_Def
     * abc_Def => abc_Def
     * abc Def => abc_Def
     * abc$ => abc_
     * ABC_DEF => ABC_DEF
     */
    public static String camelCaseToUnderscoreSeparated(
        final String camelCase) {
        return camelCase.trim()
            .replaceAll("\\s+", "_")
            .replaceAll("\\W+", "_") // non-word chars become underscores too
            .replaceAll("([a-z0-9])([A-Z])", "$1_$2")
            .replaceAll("([0-9])([a-z])", "$1_$2")
;
    }

    public static String variableName2TableName(final String variableName) {
        return camelCaseToUnderscoreSeparated(variableName).toUpperCase();
    }

    /**
     * abc => Abc
     * abc_def => Abc def (db style column names)
     * ABC_DEF => Abc def (db style column names)
     * abc.def => Abc def (R style variables)
     * ABC.DEF => Abc def (R style variables)
     * abcDef => Abc def (camel case)
     * @param variableName
     * @return
     */
    public static String variableName2Caption(final String variableName) {
        return variableName2Caption(variableName, false, true);
    }

    /**
     * abc => Abc
     * abc_def => Abc Def (db style column names)
     * ABC_DEF => Abc Def (db style column names)
     * abc.def => Abc Def (R style variables)
     * ABC.DEF => Abc Def (R style variables)
     * abcDef => Abc Def (camel case)
     * 5minutes => 5 Minutes
     * _5minutes => 5 Minutes
     * @param variableName
     * @return
     */
    public static String variableName2Title(final String variableName) {
        return variableName2Caption(variableName, true, true);
    }

    public static String variableName2Caption(final String variableName,
        boolean capitalizeAllWords, boolean replaceDots) {
        String ret = camelCaseToUnderscoreSeparated(
            variableName.replaceFirst("^_", "")).toLowerCase();
        ret = ret.replace('_', ' ');
        if (replaceDots) {
            ret = ret.replace('.', ' ');
        }
        if (capitalizeAllWords) {
            return org.apache.commons.lang.WordUtils.capitalize(ret);
        } else {
            return org.apache.commons.lang.StringUtils.capitalize(ret);
        }
    }

    /**
     * Replaces all non-word characters with _
     * @param caption
     * @return String
     */
    public static String caption2VariableName(final String caption) {
        if (caption.length() <= 0) {
            return "x";
        }

        char[] characters = caption.toCharArray();
        if (!Character.isLetter(characters[0])) {
            characters[0] = 'x';
        }
        for (int i = 1; i < characters.length; i++) {
            if (!Character.isLetterOrDigit(characters[i])) {
                characters[i] = '_';
            }
        }

        return new String(characters);
    }

    public static String centerJustifyString(final String s, final int w) {
        return centerJustifyString(s, w, ' ');
    }

    public static String centerJustifyString(String s, final int w,
        final char fillChar) {
        if (s == null) {
            s = "";
        }
        if (w <= s.length()) {
            return truncate(s, w);
        }
        int left = Math.round((w - s.length() + 1) / 2);
        int right = w - s.length() - left;
        return repeatChar(fillChar, left) + s + repeatChar(fillChar, right);
    }

    /**
     * Chops off the last character of the stringbuffer
     * @param sb
     */
    public static void chop(final StringBuffer sb) {
        sb.setLength(Math.max(sb.length() - 1, 0));
    }

    /**
     * @param sb
     * @param numberOfCharsToChopOff The number of characters to chop off
     */
    public static void chop(final StringBuffer sb,
        final int numberOfCharsToChopOff) {
        sb.setLength(Math.max(sb.length() - numberOfCharsToChopOff, 0));
    }

    protected static String double2HumanreadableString(double dbl,
        final int decimals) {
        return String.format("%,." + decimals + "f", dbl);
        // broken! see unit test
        // StringBuilder sb = new StringBuilder();
        // boolean negative = dbl < 0;
        // if (negative) {
        // dbl = -dbl;
        // }
        // double pow = Math.pow(10, decimals);
        // dbl = Math.round(dbl * pow);
        // if (pow == 0) {
        // sb.append("0");
        // } else {
        // int i = 0;
        // while (dbl > 0) {
        // if (i == decimals) {
        // sb.append(".");
        // }
        // sb.append((long) dbl % 10);
        // dbl = (long) dbl / 10;
        // i++;
        // }
        // if (i == decimals) {
        // sb.append(".0");
        // }
        // if (negative) {
        // sb.append("-");
        // }
        // }
        //
        // return sb.reverse().toString();
    }

    public static String getEndLine(final String heading) {
        return getEndLine(heading, HORIZONTAL_LINE_WIDTH);
    }

    public static String getEndLine(final String heading,
        final int horisontalLineWidth) {
        return getHorisontalLineWithText("End: " + heading, '-',
            horisontalLineWidth);
    }

    public static String getHorisontalLineWithText(final String text) {
        return getHorisontalLineWithText(text, '-');
    }

    /**
     * @param text
     * @param fillChar
     * @return
     */
    public static String getHorisontalLineWithText(final String text,
        final char fillChar) {
        return getHorisontalLineWithText(text, fillChar, HORIZONTAL_LINE_WIDTH);
    }

    public static String getHorisontalLineWithText(final String text,
        final char fillChar,
        final int horisontalLineWidth) {

        StringTokenizer st = new StringTokenizer(text, "\n\r");
        if (st.hasMoreElements()) {
            String cur_text = st.nextToken();
            if (st.hasMoreElements()) {
                // we have newlines!
                StringBuffer ret =
                    new StringBuffer(getHorisontalLineWithText(cur_text,
                        fillChar, horisontalLineWidth));
                while (st.hasMoreElements()) {
                    ret.append("\n");
                    ret.append(getHorisontalLineWithText(st.nextToken(),
                        fillChar,
                        horisontalLineWidth));
                }
                return ret.toString();
            }
        }

        if (text.length() > horisontalLineWidth - 4) {
            int last_space_index = text.substring(0, horisontalLineWidth - 4)
                .lastIndexOf(' ');
            if (last_space_index > 0
                && last_space_index < horisontalLineWidth - 4) {
                return getHorisontalLineWithText(text.substring(0,
                    last_space_index),
                    fillChar, horisontalLineWidth)
                    + "\n"
                    + getHorisontalLineWithText(text
                        .substring(last_space_index + 1),
                        fillChar, horisontalLineWidth);
            } else {
                // int split_index = horisontalLineWidth-4;
                int split_index = text.length() / 2;
                return getHorisontalLineWithText(
                    text.substring(0, split_index),
                    fillChar, horisontalLineWidth)
                    + "\n"
                    + getHorisontalLineWithText(text.substring(split_index),
                        fillChar,
                        horisontalLineWidth);
            }
        } else {
            return centerJustifyString(" " + text + " ", horisontalLineWidth,
                fillChar);
        }
    }

    /**
     * Generates a string exactly the length i, with the values
     * 012345678901234...
     * @param i
     * @return
     */
    public static String getNumbersString(final int i) {
        StringBuffer sb = new StringBuffer(i);
        for (int j = 0; j < i; j++) {
            sb.append(String.valueOf((j % 10)));
        }
        return sb.toString();
    }

    /**
     * Obtain the StackTrace from a Throwable,
     * and return it as astring.
     * @param e
     * @return StackTrace as a String
     */
    public static String getStackTraceStringFromThrowable(final Throwable e) {
        StringWriter sw = new StringWriter();
        e.printStackTrace(new PrintWriter(sw));
        return sw.toString();
    }

    public static String getStartLine(final String heading) {
        return getStartLine(heading, HORIZONTAL_LINE_WIDTH);
    }

    public static String getStartLine(final String heading,
        final int horisontalLineWidth) {
        return getHorisontalLineWithText("Start: " + heading, '-',
            horisontalLineWidth);
    }

    /**
     * @param clazz
     * @return The classname without the packagename prepended.
     */
    public static String getUnqualifiedClassName(final Class<?> clazz) {
        return getUnqualifiedClassName(clazz.getName());
    }

    /**
     * @param class_name
     * @return The classname without the packagename prepended
     */
    public static String getUnqualifiedClassName(final String class_name) {
        return class_name.substring(class_name.lastIndexOf('.') + 1);
    }

    /**
     * join with ","
     */
    public static String csv(final Object... objs) {
        return join(Arrays.asList(objs), ",");
    }

    /**
     * join with ", "
     */
    public static String commaSeperate(final Object... objs) {
        return join(Arrays.asList(objs), ", ");
    }

    public static String join(final Collection<?> objs) {
        return join(objs, ",");
    }

    public static String join(final Collection<?> objs,
        final String seperator) {
        StringBuffer ret = new StringBuffer();
        if (objs == null) {
            return "";
        }
        Iterator<?> iter = objs.iterator();
        if (iter.hasNext()) {
            ret.append(iter.next().toString());
            while (iter.hasNext()) {
                ret.append(seperator);
                ret.append(iter.next().toString());
            }
        }
        return ret.toString();
    }

    public static String join(final Object[] objs) {
        return join(objs, ",");
    }

    /**
     * Joins an array of objects into a string.
     * @param objs Array of objects to be converted with toString().
     * @param seperator String used to delimit the array of objects.
     * @return String composed of delimeted string representations of the array
     *         of objects. (like perl join)
     */
    public static String join(final Object[] objs, final String seperator) {
        return join(Arrays.asList(objs), seperator);
    }

    public static String leftJustifyString(final String s, final int w) {
        return leftJustifyString(s, w, ' ');
    }

    public static String leftJustifyString(String s, final int w,
        final char fillChar) {
        if (s == null) {
            s = "";
        }
        if (w <= s.length()) {
            return truncate(s, w);
        }
        return s + repeatChar(fillChar, w - s.length());
    }

    public static String nullString2EmptyString(final String str) {
        if (str == null) {
            return "";
        } else {
            return str;
        }
    }

    public static void printEndLine(final String heading) {
        System.out.println(getEndLine(heading));
    }

    public static void printHorisontalLineWithText(final String text) {
        System.out.println(getHorisontalLineWithText(text, '-'));
    }

    public static void printStartLine(final String heading) {
        System.out.println(getStartLine(heading));
    }

    /**
     * Prints the output of verticalyDelimit (see <b>verticalyDelimit</b>).
     * @param heading
     * @param body
     */
    public static void printVerticalyDelimited(final String heading,
        final String body) {
        System.out.println(verticalyDelimit(heading, body));
    }

    /**
     * Concatedates the character c with itself for the indicated amount
     * @param c
     * @param times
     * @return
     */
    public static String repeatChar(final char c, final int times) {
        return repeatString(String.valueOf(c), times);
    }

    /**
     * Concatedates the string s with itself for the indicated amount
     * @param s
     * @param times
     * @return
     */
    public static String repeatString(String s, int times) {
        if (s == null) {
            s = "";
        }
        if (times < 0) {
            times = 0;
        }
        StringBuffer sb = new StringBuffer(times);
        for (int i = 0; i < times; i++) {
            sb.append(s);
            // sb.append( String.valueOf( (i%10) ) );
        }
        return sb.toString();
    }

    public static String rightJustifyInt(final int number, final int w) {
        return StringUtils.rightJustifyString(String.valueOf(number), w, '0');
    }

    public static String rightJustifyString(final String s, final int w) {
        return rightJustifyString(s, w, ' ');
    }

    public static String rightJustifyString(String s, final int w,
        final char fillChar) {
        if (s == null) {
            s = "";
        }
        if (w <= s.length()) {
            return truncate(s, w);
        }
        return repeatChar(fillChar, w - s.length()) + s;
    }

    public static String tableName2CamelCase(final String tableName) {
        return tableName2CamelCase(tableName, true);
    }

    public static String tableName2CamelCase(final String tableName,
        final boolean firstLetterCapital) {
        return tableName2CamelCase(tableName, firstLetterCapital, false);
    }

    /**
     * @param tableName
     * @param firstLetterCapital
     * @param insert_spaces If true, spaces will be added for all non-letters
     * @return
     */
    public static String tableName2CamelCase(String tableName,
        final boolean firstLetterCapital, final boolean insert_spaces) {
        tableName = tableName.trim();
        StringBuffer ret = new StringBuffer();
        int i = 0;
        // read past invalid leading characters
        while (i < tableName.length()
            && !Character.isLetter(tableName.charAt(i))) {
            i++;
        }

        // do first letter
        if (i < tableName.length() && Character.isLetter(tableName.charAt(i))) {
            if (firstLetterCapital) {
                ret.append(Character.toUpperCase(tableName.charAt(i)));
            } else {
                ret.append(Character.toLowerCase(tableName.charAt(i)));
            }
            i++;
        }

        // do rest
        boolean next_capital = false;
        while (i < tableName.length()) {
            if (!Character.isLetterOrDigit(tableName.charAt(i))) {
                next_capital = true;
                if (insert_spaces) {
                    ret.append(" ");
                }
                i++;
            } else {
                if (next_capital) {
                    ret.append(Character.toUpperCase(tableName.charAt(i)));
                } else {
                    ret.append(Character.toLowerCase(tableName.charAt(i)));
                }
                next_capital = false;
                i++;
                if (i < tableName.length()) {
                    if (Character.isLowerCase(tableName.charAt(i - 1))
                        && Character
                            .isUpperCase(tableName.charAt(i))) {
                        next_capital = true;
                    }
                }
            }
        }

        return ret.toString();
    }

    /**
     * Truncates 'str' so that it contains at most 'length' characters,
     * but giving the last part of the string.
     * @param str
     * @param length
     * @return
     */
    public static String tail(final String str, final int length) {
        if (str.length() > length) {
            return str.substring(str.length() - length);
        } else {
            return str;
        }
    }

    /**
     * Truncates 'str' so that it contains at most 'length' characters.
     * @param str
     * @param length
     * @return
     */
    public static String truncate(final String str, final int length) {
        if (str.length() > length) {
            return str.substring(0, length);
        } else {
            return str;
        }
    }

    /**
     * Truncates 'str' so that it contains at most 'length' characters.
     * and add an elipse if neccessary
     * @param str
     * @param length
     * @param elipse
     * @return
     */
    public static String truncate(final String str, final int length,
        final String elipse) {
        if (str.length() <= length) {
            return str;
        } else {
            return truncate(str, length - elipse.length()) + elipse;
        }
    }

    public static String underscores2Spaces(final String str) {
        return StringUtils.nullString2EmptyString(str).replaceAll("_", " ");
    }

    /**
     * Returns the string with a header: "==== Start: myheader ===="
     * and a footer: "==== End: myheader ====" added.
     * Try it out to see what it looks like.
     * @param heading (will get trimmed)
     * @param body
     * @return
     */
    public static String verticalyDelimit(final String heading,
        final String body) {
        return verticalyDelimit(heading, body, true, HORIZONTAL_LINE_WIDTH);
    }

    public static String verticalyDelimit(String heading, final String body,
        final boolean printWhenEmpty, final int horisontalLineWidth) {
        heading = heading.trim();
        if (body.length() == 0) {
            if (printWhenEmpty) {
                return getHorisontalLineWithText(heading, '=',
                    horisontalLineWidth);
            } else {
                return "";
            }
        }

        StringBuffer ret = new StringBuffer();
        appendLine(ret, getStartLine(heading, horisontalLineWidth));
        appendLine(ret, body);
        appendLine(ret, getEndLine(heading, horisontalLineWidth));
        return ret.toString();
    }

    /** Creates a new instance of Utils */
    public StringUtils() {
    }

    public static String abbreviateMiddle(String text, int length) {
        return org.apache.commons.lang.StringUtils.abbreviateMiddle(
            text, "...", length);
    }


}
