package za.co.enerweb.toolbox.string;

import lombok.Delegate;

/**
 * Simple String builder, mainly needed for gwt usage.
 */
public class StringBuilderUtil {
    //@Delegate
    private StringBuilder sb = new StringBuilder();

    public StringBuilderUtil add(String val) {
    	
        sb.append(val);
        return this;
    }

    public StringBuilderUtil add(Double val) {
        sb.append(val);
        return this;
    }

    public StringBuilderUtil add(double val) {
        sb.append(val);
        return this;
    }

    public StringBuilderUtil add(double[] values) {
        for (double d : values) {
            add(d);
        }
        return this;
    }

    public StringBuilderUtil add(Integer val) {
        sb.append(val);
        return this;
    }

    public StringBuilderUtil add(int val) {
        sb.append(val);
        return this;
    }

    public StringBuilderUtil add(Boolean val) {
        sb.append(val);
        return this;
    }

    public StringBuilderUtil add(boolean val) {
        sb.append(val);
        return this;
    }

    public StringBuilderUtil newline() {
        sb.append('\n');
        return this;
    }

    public StringBuilderUtil add(final String txt,
        final int tabs) {
        sb.append(StringUtils.repeatString("\t", tabs));
        sb.append(txt);
        return this;
    }

    /**
     * Appends the message and the stack trace to the string buffer.
     * @param sb
     * @param e
     */
    public StringBuilderUtil add(final Throwable e) {
        addLine(e.getMessage());
        addLine(StringUtils.getStackTraceStringFromThrowable(e));
        return this;
    }

    public StringBuilderUtil addLine(final String line) {
        sb.append(line);
        sb.append("\n");
        return this;
    }

    public StringBuilderUtil addLine(final String line,
        final int tabs) {
        sb.append(StringUtils.repeatString("\t", tabs));
        sb.append(line);
        sb.append("\n");
        return this;
    }

    /**
     * appends text with trailing space
     * @param sb
     * @param str
     */
    public StringBuilderUtil addWord(final String str) {
        sb.append(str);
        sb.append(" ");
        return this;
    }

    /**
     * appends both strings and then a trailing space
     * @param sb
     * @param str
     * @param punctuation
     */
    public StringBuilderUtil addWord(final String str,
        final String punctuation) {
        sb.append(str);
        sb.append(punctuation);
        sb.append(" ");
        return this;
    }

    public String toString() {
        return sb.toString();
    }

}
