package za.co.enerweb.toolbox.crypto;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.math.BigInteger;
import java.security.Key;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.MessageDigest;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.RSAPrivateKeySpec;
import java.security.spec.RSAPublicKeySpec;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import za.co.enerweb.toolbox.string.HexString;

@Slf4j
public class CryptoUtils {

    public static final int BLOWFISH_BITS = 128; // why so small :(
    public static final int BLOWFISH_BYTES = BLOWFISH_BITS / 8;

    @SneakyThrows
    public static String sha256(byte[] data) {
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        md.update(data);
        return new String(HexString.bytes2HexString(md.digest()));
    }

    @SneakyThrows
    public static byte[] encryptAes(byte[] data, byte[] key) {
        return encrypt(data, getSymmetricKey(key, "AES"), "AES");
    }

    @SneakyThrows
    public static byte[] decryptAes(byte[] data, byte[] key) {
        return decrypt(data, getSymmetricKey(key, "AES"), "AES");

    }

    /**
     * No need to close the returned stream
     */
    @SneakyThrows
    public static InputStream decryptAesAsStream(byte[] data, byte[] key) {
        return new ByteArrayInputStream(decryptAes(data, key));
    }

    @SneakyThrows
    public static byte[] encryptBlowfish(byte[] data, Key key) {
        return encrypt(data, key, "Blowfish");
    }

    @SneakyThrows
    public static byte[] decryptBlowfish(byte[] data, Key key) {
        return decrypt(data, key, "Blowfish");
    }

    @SneakyThrows
    public static byte[] decryptBlowfish(byte[] data, byte[] key) {
        return decrypt(data, getSymmetricKey(key, "Blowfish"), "Blowfish");
    }

    @SneakyThrows
    public static byte[]
        encryptRsa(byte[] data, byte[] key, boolean privateKey) {
        return encrypt(data, getAsymmetricKey(key, "RSA", privateKey),
            "RSA/ECB/PKCS1Padding");
    }

    @SneakyThrows
    public static byte[]
        decryptRsa(byte[] data, byte[] key, boolean privateKey) {
        return decrypt(data, getAsymmetricKey(key, "RSA", privateKey),
            "RSA/ECB/PKCS1Padding");
    }

    @SneakyThrows
    public static byte[] encrypt(byte[] data, Key key,
        String cypherAlgorithm) {
        Cipher c = Cipher.getInstance(cypherAlgorithm);
        c.init(Cipher.ENCRYPT_MODE, key);
        byte[] encVal = c.doFinal(data);
        return encVal;
    }

    @SneakyThrows
    public static byte[] decrypt(byte[] encryptedData, Key key,
        String cypherAlgorithm) {
        Cipher c = Cipher.getInstance(cypherAlgorithm);
        c.init(Cipher.DECRYPT_MODE, key);
        return c.doFinal(encryptedData);
    }

    private static Key getSymmetricKey(byte[] key, String algorithm) {
        Key ret = new SecretKeySpec(key, algorithm);
        return ret;

    }

    @SneakyThrows
    private static Key getAsymmetricKey(byte[] key, String algorithm,
        boolean privateKey) {
        KeyFactory keyFactory = KeyFactory.getInstance(algorithm);
        if (privateKey) {
            PKCS8EncodedKeySpec spec2 = new PKCS8EncodedKeySpec(key);
            return keyFactory.generatePrivate(spec2);
        } else {
            X509EncodedKeySpec spec = new X509EncodedKeySpec(key);
            return keyFactory.generatePublic(spec);
        }
    }

    @SneakyThrows
    public static Key getBlowfishSyncKey() {
        KeyGenerator keyGenerator = KeyGenerator.getInstance("Blowfish");
        keyGenerator.init(BLOWFISH_BITS);
        Key blowfishKey = keyGenerator.generateKey();
        return blowfishKey;
    }

    @SneakyThrows
    public static byte[] sign(byte[] data, PrivateKey key) {
        Signature signer = Signature.getInstance("SHA1withRSA");
        signer.initSign(key); // PKCS#8 is preferred
        signer.update(data);
        return signer.sign();
    }

    public static boolean verifySigniature(byte[] data, PublicKey key,
        byte[] signiature) {
        try {
            Signature signer = Signature.getInstance("SHA1withRSA");
            signer.initVerify(key); // PKCS#8 is preferred
            signer.update(data);
            return signer.verify(signiature);
        } catch (Exception e) {
            log.warn("Invalid Signiature", e);
        }
        return false;
    }

    @SneakyThrows
    public static KeyPair generateKeyPairRSA() {
        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
        keyPairGenerator.initialize(1024);
        return keyPairGenerator.genKeyPair();
    }

    @SneakyThrows
    public static PublicKey getPublicKey(byte[] modulus, byte[] exponent) {
        // https://stackoverflow.com/questions/27187461/how-to-build-a-rsa-key-with-a-modulus-of-64-bytes-in-java
        // https://stackoverflow.com/questions/18819095/how-insert-bits-into-block-in-java-cryptography/18824707#18824707
        RSAPublicKeySpec spec = new RSAPublicKeySpec(
            new BigInteger(1, modulus), new BigInteger(1, exponent));
        KeyFactory factory = KeyFactory.getInstance("RSA");
        PublicKey ret = factory.generatePublic(spec);
        return ret;
    }

    @SneakyThrows
    public static PrivateKey
        getPrivateKey(byte[] modulus, byte[] privateExponent) {
        // https://stackoverflow.com/questions/27187461/how-to-build-a-rsa-key-with-a-modulus-of-64-bytes-in-java
        // https://stackoverflow.com/questions/18819095/how-insert-bits-into-block-in-java-cryptography/18824707#18824707
        RSAPrivateKeySpec spec = new RSAPrivateKeySpec(
            new BigInteger(1, modulus), new BigInteger(1, privateExponent));
        KeyFactory factory = KeyFactory.getInstance("RSA");
        PrivateKey ret = factory.generatePrivate(spec);
        return ret;
    }

    public static byte[] decodeBase64(String data) {
        return DatatypeConverter.parseBase64Binary(data);
    }

    public static String encodeBase64(byte[] data) {
        return DatatypeConverter.printBase64Binary(data);
    }
}
