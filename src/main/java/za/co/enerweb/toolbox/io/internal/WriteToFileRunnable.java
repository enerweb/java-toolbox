package za.co.enerweb.toolbox.io.internal;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

public class WriteToFileRunnable implements Runnable {

    OutputStream stream;
    String string;

    WriteToFileRunnable(final OutputStream stream, final String string) {
        this.stream = stream;
        this.string = string;
    }

    public void run() {
        BufferedWriter buffer = new BufferedWriter(
                new OutputStreamWriter(stream));

        try {
            buffer.write(string); // header
        } catch (IOException e1) {
            e1.printStackTrace();
        }

        try {
            buffer.flush();
            buffer.close();
        } catch (IOException e) {
            System.err.println("IO Exception while flushing: \n"
                + e.getMessage() + "\n");
        }

    }

}
