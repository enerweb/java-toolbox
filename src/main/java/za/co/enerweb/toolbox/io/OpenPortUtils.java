package za.co.enerweb.toolbox.io;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.Random;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public abstract class OpenPortUtils {
    private static final int PORT_INCREMENT = 7 + new Random().nextInt(93);

    /*
     * http://stackoverflow.com/questions/7144401/how-can-i-find-an-open-ports-in-range-of-ports
     */
    @SneakyThrows
    public static int getAvailablePort(int defaultPort) {
        int port = defaultPort;
        if (port > 0 && isPortAvailable(port, false)) {
            return port;
        }

        for (port += PORT_INCREMENT; port < 30000; port += PORT_INCREMENT) {
            if (isPortAvailable(port, true)) {
                log.warn("!!! using alternative port " + defaultPort + " => "
                    + port);
                return port;
            }
        }
        throw new RuntimeException("Chould not find an open alternative port "
            + "for " + defaultPort + "\n tried incrementing it by "
            + PORT_INCREMENT
            + " all the way up to " + port);
    }

    public static boolean isPortAvailable(final int port) throws IOException {
        return isPortAvailable(port, false);
    }

    private static boolean isPortAvailable(final int port, boolean logeerr)
        throws IOException {
        ServerSocket ss = null;
        try {
            ss = new ServerSocket(port);
            ss.setReuseAddress(true);
            return true;
        } catch (final IOException e) {
            if (logeerr) {
                log.debug("Could not bind to port " + port + " "
                    + e.getMessage());
            }
        } finally {
            if (ss != null) {
                ss.close();
            }
        }
        return false;
    }
}
