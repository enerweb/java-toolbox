package za.co.enerweb.toolbox.io;

/**
 * interface to provide line by line output to the caller
 */
public interface OutputHandler {
    void handleLine(String line);

    void clear();
}
