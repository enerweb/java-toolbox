package za.co.enerweb.toolbox.io;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import lombok.extern.slf4j.Slf4j;

import org.apache.commons.io.IOUtils;

@Slf4j
public class ZipUtils {
    static final int BUFFER_SIZE = 2048;

    protected static void _zipByteArray(final ZipOutputStream out,
        final byte[] data, final String path_and_filename) {
        // log.debug("Adding file " + path_and_filename);
        try {
            ZipEntry entry = new ZipEntry(path_and_filename);
            out.putNextEntry(entry);
            out.write(data, 0, data.length);
        } catch (IOException e) {
            log.error("An error occured while zipping file: "
                + path_and_filename);
            e.printStackTrace();
        }
    }

    protected static void _zipRecursively(final ZipOutputStream out,
        final File currentSourceFile, final String path, final byte buffer[]) {
        if (currentSourceFile.isDirectory()) {
            // log.debug("Zipping directory " + currentSourceFile);
            File[] childDirs = currentSourceFile.listFiles();
            for (File file : childDirs) {
                _zipRecursively(out, file, path + currentSourceFile.getName()
                    + File.separator, buffer);
            }
        } else {
            try {
                FileInputStream fi = new FileInputStream(currentSourceFile);
                BufferedInputStream origin = new BufferedInputStream(fi,
                    BUFFER_SIZE);
                _zipStream(out, origin, (path == null ? "" : path)
                    + currentSourceFile.getName(), buffer);
            } catch (FileNotFoundException e) {
                log.error("An error occured while zipping file: "
                    + currentSourceFile.getPath(), e);
            }

        }
    }

    protected static void _zipStream(final ZipOutputStream out,
        final InputStream _sourceStream, final String path_and_filename,
        final byte buffer[]) {
        // log.debug("Adding file " + path_and_filename);
        try {
            BufferedInputStream origin = new BufferedInputStream(_sourceStream,
                BUFFER_SIZE);
            // System.out.println("entry:"+ path + sourceFile.getName());
            ZipEntry entry = new ZipEntry(path_and_filename);
            out.putNextEntry(entry);
            int count;
            while ((count = origin.read(buffer, 0, buffer.length)) != -1) {
                out.write(buffer, 0, count);
            }
            origin.close();
        } catch (IOException e) {
            log.error("An error occured while zipping file: "
                + path_and_filename, e);
        }
    }

    public static List<String> getEntryNameList(final File zipFile)
        throws IOException {
        ZipFile zipFileObject = new ZipFile(zipFile);
        Enumeration<? extends ZipEntry> entries = zipFileObject.entries();
        ArrayList<String> ret = new ArrayList<String>();
        while (entries.hasMoreElements()) {
            ZipEntry entry = entries.nextElement();
            ret.add(entry.getName());
        }
        zipFileObject.close();
        return ret;
    }

    public static void unZip(final File sourceFile, final File destinationDir)
        throws IOException {
        ZipFile zipFile = new ZipFile(sourceFile);
        Enumeration<? extends ZipEntry> entries = zipFile.entries();
        while (entries.hasMoreElements()) {
            ZipEntry entry = entries.nextElement();
            String currentEntry = entry.getName();
            File destFile = new File(destinationDir, currentEntry);
            if (entry.isDirectory()) {
                destFile.mkdirs();
            } else if (!entry.isDirectory()) {
                File destinationParent = destFile.getParentFile();

                // create the parent directory structure if needed
                // just for in case they were out of order in the zip
                destinationParent.mkdirs();

                FileOutputStream output = new FileOutputStream(destFile);
                IOUtils.copy(zipFile.getInputStream(entry), output);
                output.close();
            }
        }
        zipFile.close();
    }

    /**
     *
     * @param source
     * @param destinationDir does not need to exist yet
     * @throws IOException
     */
    public static void unZip(final InputStream source,
        final File destinationDir) throws IOException {
        ZipInputStream zipStream = new ZipInputStream(source);
        ZipEntry entry;
        while ((entry = zipStream.getNextEntry()) != null) {
            String currentEntry = entry.getName();
            File destFile = new File(destinationDir, currentEntry);
            if (entry.isDirectory()) {
                destFile.mkdirs();
            } else if (!entry.isDirectory()) {
                File destinationParent = destFile.getParentFile();

                // create the parent directory structure if needed
                // just for in case they were out of order in the zip
                destinationParent.mkdirs();

                FileOutputStream output = new FileOutputStream(destFile);
                IOUtils.copy(zipStream, output);
                output.close();
            }
        }
        zipStream.close();
        source.close();
    }

    /**
     * Zip a file or a directory recursively to the target file.
     * By default it adds a root directory.
     * @param sourceFile
     * @param targetFile
     * @throws IOException
     */
    public static void zipRecursively(final File sourceFile,
        final File targetFile) throws IOException {
        zipRecursively(sourceFile, targetFile, true);
    }

    public static void zipRecursively(final File sourceFile,
        final File targetFile, final boolean includeRoot) throws IOException {
        FileOutputStream dest = new FileOutputStream(targetFile);
        ZipUtils ret = zipRecursively(sourceFile,
            new BufferedOutputStream(dest), includeRoot);
        ret.cleanup();
        dest.flush();
        dest.close();
    }

    /**
     * Zip a file or directory recursively to the output stream (spawns a thread
     * that writes data until finished) you have to call join on the returned
     * thread and close the stream.
     * By default it adds a root directory.
     * @param outputstream
     * @param sourceFile
     * @return
     * @throws IOException
     */
    public static ZipUtils zipRecursively(final File sourceFile,
        final OutputStream outputstream) throws IOException {
        return zipRecursively(sourceFile, outputstream, true);
    }

    public static ZipUtils zipRecursively(final File sourceFile,
        final OutputStream outputstream, final boolean includeRoot)
        throws IOException {
        ZipUtils ret = new ZipUtils(outputstream);
        if (includeRoot) {
            ret.addFileRecursively(sourceFile);
        } else {
            File[] files = sourceFile.listFiles();
            if (files == null) {
                throw new IOException(sourceFile.getPath()
                    + " does not exist or is not a directory.");
            }
            for (File file : files) {
                ret.addFileRecursively(file);
            }
        }
        return ret;
    }

    private byte[] buffer;

    protected ZipOutputStream out;

    protected Thread currentThread = null;

    public ZipUtils(final OutputStream outputstream) {
        out = new ZipOutputStream(outputstream);
    }

    public void addByteArray(final byte[] data,
        final String path_and_filename) {
        waitForCurrentThread();
        currentThread = new Thread(new Runnable() {
            public void run() {
                _zipByteArray(out, data, path_and_filename);
            }
        });
        currentThread.setDaemon(true);
        currentThread.start();
    }

    public void addFileRecursively(final File sourceFile) {
        waitForCurrentThread();
        currentThread = new Thread(new Runnable() {
            public void run() {
                _zipRecursively(out, sourceFile, "", getBuffer());
            }
        });
        currentThread.setDaemon(true);
        currentThread.start();
    }

    public void addStream(final InputStream inputStream,
        final String path_and_filename) {
        waitForCurrentThread();
        currentThread = new Thread(new Runnable() {
            public void run() {
                _zipStream(out, inputStream, path_and_filename, getBuffer());
            }
        });
        currentThread.setDaemon(true);
        currentThread.start();
    }

    public void cleanup() {
        waitForCurrentThread();
        buffer = null;
        if (out != null) {
            try {
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            out = null;
        }
    }

    @Override
    protected void finalize() throws Throwable {
        cleanup();
    }

    protected byte[] getBuffer() {
        if (buffer == null) {
            buffer = new byte[BUFFER_SIZE];
        }
        return buffer;
    }

    public Thread getCurrentThread() {
        return currentThread;
    }

    public ZipOutputStream getOut() {
        return out;
    }

    public void setCurrentThread(final Thread currentThread) {
        this.currentThread = currentThread;
    }

    public void setOut(final ZipOutputStream out) {
        this.out = out;
    }

    public void waitForCurrentThread() {
        if (currentThread != null && currentThread.isAlive()) {
            // DebugMode.debugOutPrintln(
            // "Waiting for other threads, before writing this data.");
            try {
                currentThread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        currentThread = null;
    }

}
