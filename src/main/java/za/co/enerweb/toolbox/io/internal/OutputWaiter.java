package za.co.enerweb.toolbox.io.internal;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import za.co.enerweb.toolbox.io.OutputHandler;

/**
 * internal class to process output streams from running a command
 */
@Data
@Slf4j
public class OutputWaiter extends Thread {

    private OutputHandler outputHandler;
    private BufferedReader reader;

    public OutputWaiter(final InputStream inputStream,
        final OutputHandler outputHandler) {
        this.outputHandler = outputHandler;

        reader = new BufferedReader(new InputStreamReader(inputStream));

        //setPriority(Thread.MIN_PRIORITY);
        setPriority(Math.max(Thread.currentThread().getPriority() - 2,
            Thread.MIN_PRIORITY));
        setDaemon(true);
    }

    @Override
    public void run() {
        try {
            String line;
            while ((line = reader.readLine()) != null) {
                outputHandler.handleLine(line);
            }
        } catch (IOException ioe) {
            log.info("Error while waiting for stream", ioe);
        }
    }
}
