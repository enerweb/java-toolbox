package za.co.enerweb.toolbox.io;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

/**
 * Just sends the output to the logger
 */
@Data
@Slf4j
public class LogOutputHandler implements OutputHandler {

    @Override
    public void handleLine(final String line) {
        log.info(line);
    }

    public void clear() {
    }

}
