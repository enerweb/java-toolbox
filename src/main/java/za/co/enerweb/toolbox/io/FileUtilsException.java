/*
 * Created on Oct 7, 2004
 */
package za.co.enerweb.toolbox.io;

/**
 * @author Marius Kruger
 *         Soli deo gloria.
 */
public class FileUtilsException extends Exception {

    private static final long serialVersionUID = 1L;

    /**
     * 
     */
    public FileUtilsException() {
        super();
    }

    /**
     * @param message
     */
    public FileUtilsException(String message) {
        super(message);
    }

    /**
     * @param message
     * @param cause
     */
    public FileUtilsException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * @param cause
     */
    public FileUtilsException(Throwable cause) {
        super(cause);
    }

}
