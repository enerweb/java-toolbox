package za.co.enerweb.toolbox.io;

import java.io.File;
import java.io.IOException;

import lombok.extern.slf4j.Slf4j;
import za.co.enerweb.toolbox.id.IdGenerator;

/**
 * Util for creating directories in the system temp dir.
 */
@Slf4j
public final class TempDir {

    private TempDir() {
    }

    /**
     * Create a directory in java.io.tmpdir with a guid name
     * Will be deleted by the JVM if it exist normally
     * @return
     */
    public static File createTempDir() {
        return createTempDir(true);
    }

    /**
     * Create a directory in java.io.tmpdir with a guid name
     * @param deleteOnExit
     * @return
     */
    public static File createTempDir(final boolean deleteOnExit) {
        return createTempDir("TempDir." + IdGenerator.getLocallyUniqueId(),
            deleteOnExit);
    }

    /**
     * Create a directory in java.io.tmpdir with the name provided
     * (if it exists, it will be deleted and re-created)
     * Will be deleted by the JVM if it exitst normally
     * @param name
     * @return
     */
    public static File createTempDir(final String name) {
        return createTempDir(name, true);
    }

    /**
     * Create a directory in java.io.tmpdir with the name provided
     * Will be re-used
     * @param name
     * @return
     */
    public static File createDirInTempDir(final String name) {
        return createTempDir(name, false);
    }

    /**
     * Create a directory in java.io.tmpdir with the name provided. If the
     * directory already exists, it will be deleted and re-created.
     * @param name
     * @param deleteOnExit If true, it will also be deleted when the system
     *            exits normally.
     * @return
     */
    public static File createTempDir(final String name,
        final boolean deleteOnExit) {
        return createTempDir(name, deleteOnExit, true);
    }

    /**
     * Create a directory in java.io.tmpdir with the name provided.
     * @param name
     * @param deleteOnExit If true, it will also be deleted when the system
     *            exits normally.
     * @param cleanNow If true it will be deleted and re-created.
     * @return
     */
    public static File createTempDir(final String name,
        final boolean deleteOnExit, final boolean cleanNow) {
        File tempDir = new File(System.getProperty("java.io.tmpdir"),
            name);

        // make sure we give tests a new clean directory
        if (cleanNow && tempDir.exists()) {
            try {
                if (!tempDir.canWrite()) {
                    // some tests can make the temp dir read-only,
                    // but we need to delete it in that case too.
                    tempDir.setWritable(true);
                }
                log.debug("Deleting temp dir now: " + tempDir);
                org.apache.commons.io.FileUtils.deleteDirectory(tempDir);
            } catch (IOException e) {
                // if we can't delete it just create one with a unique name
                return createTempDir(name + "_"
                    + IdGenerator.getLocallyUniqueId(), deleteOnExit);
            }
        }

        tempDir.mkdirs();
        if (deleteOnExit) {
            tempDir.deleteOnExit();
        }
        return tempDir;
    }

    /**
     * Creates temp dirs grouped to a predictable name and create a unique
     * second level directory
     *
     * @param name
     * @param secondLevelName
     * @param deleteOnExit
     * @return
     */
    public static File createGroupedTempDir(final String groupName,
        final String secondLevelName, final boolean deleteOnExit) {
        File tempDir = TempDir.createTempDir(groupName, deleteOnExit, false);
        File tempDirLevel2 = new File(tempDir, secondLevelName
            + "_" + IdGenerator.getLocallyUniqueId());
        // log.debug("createGroupedTempDir: " + tempDirLevel2);
        tempDirLevel2.deleteOnExit();
        tempDirLevel2.mkdirs();
        return tempDirLevel2;
    }
}
