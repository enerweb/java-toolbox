package za.co.enerweb.toolbox.io;

public enum CopyOrMove {
    COPY,
    MOVE;

    public boolean isCopy() {
        return this.equals(COPY);
    }

    public boolean isMove() {
        return this.equals(MOVE);
    }
}
