package za.co.enerweb.toolbox.io;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;


/**
 * Put the output in a a string buffer,
 * taking care that it does not grow out of control.
 */
@Data
@Slf4j
public class StringBuilderOutputHandler implements OutputHandler {
    private static final int DEFAULT_MAX_LENGTH = 10000;
    private static final int DEFAULT_TRIM_SIZE = 1000;
    private StringBuilder stringBuilder = new StringBuilder();
    /**
     * when input grows beyond this, trim it
     */
    private int maxLength = DEFAULT_MAX_LENGTH;
    /**
     * trim a little more than just a line at a time
     */
    private int trimSize = DEFAULT_TRIM_SIZE;
    private String newline = "\n";

    @Override
    public void handleLine(final String line) {
        // log.debug(line);
        stringBuilder.append(line);
        stringBuilder.append(newline);
        if (stringBuilder.length() > maxLength
            && trimSize > stringBuilder.length()) {
            stringBuilder.delete(0, trimSize);
        }
    }

    @Override
    public String toString() {
        return stringBuilder.toString();
    }

    @Override
    public void clear() {
        stringBuilder.setLength(0);
    }

}
