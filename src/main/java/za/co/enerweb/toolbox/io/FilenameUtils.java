package za.co.enerweb.toolbox.io;


public class FilenameUtils {

    /**
     * Fix up multiple paths in one go.
     * Cleans up paths properly: only returns
     * unix separators, no trailing separators
     *
     * @param paths
     */
    public static void normalizeFileNames(String... paths) {
        for (int i = 0; i < paths.length; i++) {
            paths[i] = org.apache.commons.io.FilenameUtils
                .normalizeNoEndSeparator(paths[i], true);
        }
    }
}
