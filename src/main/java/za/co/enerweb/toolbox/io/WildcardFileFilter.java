package za.co.enerweb.toolbox.io;

import java.io.File;
import java.io.FileFilter;

public class WildcardFileFilter implements FileFilter {
    String fileNameRegExpr;
    boolean trueForDirectories = false;

    /**
     * @param fileNameString a fileNameString like pre*p?st.txt
     * @throws FileUtilsException
     */
    public WildcardFileFilter(final String fileNameString)
        throws FileUtilsException {
        fileNameRegExpr =
            FileUtils.convertWildCardFileNameStringToRegularExpression(
                fileNameString);
    }

    /**
     * @param fileNameString a fileNameString like pre*p?st.txt
     * @param trueForDirectories if this is true, it will allways accept
     *        directories
     * @throws FileUtilsException
     */
    public WildcardFileFilter(final String fileNameString,
        final boolean trueForDirectories)
        throws FileUtilsException {
        this.trueForDirectories = trueForDirectories;
        fileNameRegExpr =
                FileUtils.convertWildCardFileNameStringToRegularExpression(
                    fileNameString);
    }

    /*
     * @see java.io.FileFilter#accept(java.io.File)
     */
    public boolean accept(final File pathname) {
        return trueForDirectories && pathname.isDirectory()
            || pathname.getName().matches(fileNameRegExpr);
    }

    public String getFileNameRegExpr() {
        return fileNameRegExpr;
    }

    public boolean isTrueForDirectories() {
        return trueForDirectories;
    }

    public void setFileNameRegExpr(final String fileNameRegExpr) {
        this.fileNameRegExpr = fileNameRegExpr;
    }

    public void setTrueForDirectories(final boolean trueForDirectories) {
        this.trueForDirectories = trueForDirectories;
    }
}
