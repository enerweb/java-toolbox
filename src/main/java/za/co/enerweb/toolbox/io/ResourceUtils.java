package za.co.enerweb.toolbox.io;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.JarURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import lombok.extern.slf4j.Slf4j;

import org.apache.commons.io.IOUtils;

/**
 * A utility which helps reading resources from the classpath
 */
@Slf4j
public final class ResourceUtils {

    private ResourceUtils() {
    }

    /**
     * @param resourcePath eg. "za/co/enerweb/Test.class"
     * @throws IOException
     */
    public static byte[] getResourceAsBytes(final String resourcePath)
        throws IOException {
        InputStream stream = getResourceAsStream(resourcePath);
        return IOUtils.toByteArray(stream);
    }

    /**
     * Probably just useful if you have the class file available, for example
     * when doing unit tests.
     * @param resourcePath eg. "za/co/enerweb/Test.class"
     * @throws IOException
     */
    public static File getResourceAsFile(final String resourcePath)
        throws IOException {
        URL url = getResourceAsUrl(resourcePath);
        File file = new File(url.getPath());
        if (!file.exists()) {
            throw new IOException("File does not exist: " + url);
        }
        return file;
    }

    /**
     * @param resourcePath eg. "za/co/enerweb/Test.class"
     * @throws IOException
     */
    public static Reader getResourceAsReader(final String resourcePath)
        throws IOException {
        return new InputStreamReader(getResourceAsStream(resourcePath));
    }

    /**
     * @param resourcePath eg. "za/co/enerweb/Test.class"
     * @throws IOException
     */
    public static InputStream getResourceAsStream(final String resourcePath)
        throws IOException {
        InputStream stream = Thread.currentThread().getContextClassLoader()
            .getResourceAsStream(resourcePath);
        if (stream == null) {
            // log.debug("Thread.currentThread(): " + Thread.currentThread());
            // log.debug("Thread.currentThread().getName(): "
            // + Thread.currentThread().getName());
            // log.debug("Thread.currentThread().getContextClassLoader(): "
            // + Thread.currentThread().getContextClassLoader());
            // log.debug("ResourceUtils.class.getClassLoader(): "
            // + ResourceUtils.class.getClassLoader());
            throw new IOException("Could not read resource from classpath: "
                + resourcePath);
        }
        return stream;
    }

    /**
     * @param resourcePath eg. "za/co/enerweb/Test.class"
     * @throws IOException
     */
    public static String getResourceAsString(final String resourcePath)
        throws IOException {
        InputStream stream = getResourceAsStream(resourcePath);
        return IOUtils.toString(stream);
    }

    /**
     * @param resourcePath eg. "za/co/enerweb/Test.class"
     * @throws IOException
     */
    public static URL getResourceAsUrl(final String resourcePath)
        throws IOException {
        URL url = Thread.currentThread().getContextClassLoader().getResource(
            resourcePath);
        if (url != null) {
            return url;
        }

        // if at first you don't succeed, try a little harder..
        url = ResourceUtils.class.getClassLoader().getResource(resourcePath);
        if (url != null) {
            return url;
        }

        // log.debug("looking at urls");
        //
        // // bend over backwards...
        // for (URL anUrl : ((URLClassLoader) Thread.currentThread()
        // .getContextClassLoader()).getURLs()) {
        // log.debug("looking for " + anUrl + " :: " + resourcePath);
        // String path = anUrl.getPath();
        // if (!path.isEmpty() && !path.endsWith(".jar")) {
        // File f = new File(path, resourcePath);
        // log.debug("looking for " + f + " ;; " + resourcePath);
        // if (f.exists()) {
        // return f.toURI().toURL();
        // }
        // }
        // }

        throw new IOException(
            "Could not read resource from classpath: "
                + resourcePath);
    }

    /**
     * This code was adapted from what was posted on a forum:
     * http://forums.sun.com/thread.jspa?threadID=341935&start=15&tstart=0
     * @param resourcePath eg. "za/co/enerweb" or "za.co.enerweb"
     * @return
     * @throws ClassNotFoundException
     */
    public static Collection<String> getResourcesForPackage(String resourcePath)
        throws ClassNotFoundException {
        resourcePath = resourcePath.replace('.', '/').replaceAll("/$", "");
        // This will hold a list of directories matching the package name.
        // There may be more than one if a package is split over multiple
        // jars/paths
        List<String> ret = new ArrayList<String>();
        ArrayList<File> directories = new ArrayList<File>();
        try {
            ClassLoader cld = Thread.currentThread().getContextClassLoader();
            if (cld == null) {
                throw new ClassNotFoundException("Can't get class loader.");
            }
            // Ask for all resources for the path
            Enumeration<URL> resources = cld.getResources(resourcePath);
            while (resources.hasMoreElements()) {
                URL res = resources.nextElement();
                if (res.getProtocol().equalsIgnoreCase("jar")) {
                    JarURLConnection conn = (JarURLConnection) res
                        .openConnection();
                    JarFile jar = conn.getJarFile();
                    for (JarEntry e : Collections.list(jar.entries())) {

                        String entryName = e.getName();
                        if (entryName.startsWith(resourcePath)
                            // +1 to account for trailing slash in returned
                            // directories, and we don't want the base dir back.
                            && entryName.length() > resourcePath.length() + 1
                            && !entryName.endsWith(".class")
                            && !entryName.contains("$")) {
                            String resourceName = entryName;
                            ret.add(resourceName);
                        }
                    }
                } else {
                    directories.add(new File(URLDecoder.decode(res.getPath(),
                        "UTF-8")));
                }
            }
        } catch (NullPointerException x) {
            throw new ClassNotFoundException(resourcePath
                + " does not appear to be "
                + "a valid package (Null pointer exception)");
        } catch (UnsupportedEncodingException encex) {
            throw new ClassNotFoundException(resourcePath
                + " does not appear to be "
                + "a valid package (Unsupported encoding)");
        } catch (IOException ioex) {
            throw new ClassNotFoundException(
                "IOException was thrown when trying "
                    + "to get all resources for " + resourcePath);
        }

        // For every directory identified capture all the non-.class files
        for (File directory : directories) {
            if (directory.exists()) {
                // Get the list of the files contained in the package
                String[] files = directory.list();
                for (String file : files) {
                    // we are not interested in .class files
                    if (!file.endsWith(".class")) {
                        // removes the .class extension
                        ret.add(resourcePath + '/' + file);
                    }
                }
            } else {
                throw new ClassNotFoundException(resourcePath + " ("
                    + directory.getPath()
                    + ") does not appear to be a valid package");
            }
        }
        return ret;
    }
}
