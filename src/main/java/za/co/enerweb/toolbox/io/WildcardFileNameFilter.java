package za.co.enerweb.toolbox.io;

import java.io.File;
import java.io.FilenameFilter;

public class WildcardFileNameFilter implements FilenameFilter {
    String fileNameRegExpr;

    /**
     * @param fileNameString a fileNameString like pre*p?st.txt
     * @throws FileUtilsException
     */
    public WildcardFileNameFilter(final String fileNameString)
        throws FileUtilsException {
        fileNameRegExpr =
            FileUtils.convertWildCardFileNameStringToRegularExpression(
                fileNameString);
    }

    /*
     * @see java.io.FilenameFilter#accept(java.io.File, java.lang.String)
     */
    public boolean accept(final File dir, final String name) {
        return name.matches(fileNameRegExpr);
    }

    public String getFileNameRegExpr() {
        return fileNameRegExpr;
    }

    public void setFileNameRegExpr(final String fileNameRegExpr) {
        this.fileNameRegExpr = fileNameRegExpr;
    }
}
