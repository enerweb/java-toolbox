package za.co.enerweb.toolbox.math;


public abstract class MathUtils {

    public static Double sum(Iterable<Double> values) {
        Double ret = 0d;
        for (Double d : values) {
            ret += d;
        }
        return ret;
    }

    public static double max(double... values) {
        double ret = Double.MIN_VALUE;
        for (double val : values) {
            ret = Math.max(ret, val);
        }
        return ret;
    }

    public static int max(int... values) {
        int ret = Integer.MIN_VALUE;
        for (int val : values) {
            ret = Math.max(ret, val);
        }
        return ret;
    }

    public static int min(int... values) {
        int ret = Integer.MAX_VALUE;
        for (int val : values) {
            ret = Math.min(ret, val);
        }
        return ret;
    }

    /**
     * Raise the value to the second power.
     * apparently this is faster than Math. power(x,2)
     * @param value
     * @return
     */
    public static double square(double value) {
        return value * value;
    }

    /**
     * percent / 100.0
     * % function from excel
     * @param percent
     * @return
     */
    public static double percentToRatio(double percent) {
        return percent / 100.0;
    }

    /**
     * ratio * 100.0
     * @param ratio
     * @return
     */
    public static double ratioToPercent(double ratio) {
        return ratio * 100.0;
    }

    /** round the value up to the specified decimals, in a GWT compatable way */
    public static double round(double value, int decimals) {
        // double factor = Math.pow(10, decimals);
        double factor = 1;
        for (int i = 0; i < decimals; i++) {
            factor *= 10;
        }
        return Math.round(value * factor) / factor;
    }
}
