package za.co.enerweb.toolbox.config;

import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.Set;

import lombok.extern.slf4j.Slf4j;

/**
 * XXX: can probably make this class nicer with generics
 */
@Slf4j
public class ResourceBundleUtils {

    public static boolean getBoolean(final String bundleName,
        final String propertyName) {
        return new ResourceBundleUtils(bundleName).getBoolean(propertyName);
    }

    public static boolean getBooleanWithDefault(final String bundleName,
        final String propertyName, final boolean defaultValue) {
        return new ResourceBundleUtils(bundleName).getBooleanWithDefault(
            propertyName, defaultValue);
    }

    /**
     * @param bundleName
     * @param propertyName
     * @return
     */
    public static int getInt(final String bundleName,
        final String propertyName) {
        return new ResourceBundleUtils(bundleName).getInt(propertyName);
    }

    public static int getIntWithDefault(final String bundleName,
        final String propertyName, final int defaultValue) {
        return new ResourceBundleUtils(bundleName).getIntWithDefault(
            propertyName, defaultValue);
    }

    public static long getLong(final String bundleName,
        final String propertyName) {
        return new ResourceBundleUtils(bundleName).getLong(propertyName);
    }

    public static long getLongWithDefault(final String bundleName,
        final String propertyName, final long defaultValue) {
        return new ResourceBundleUtils(bundleName).getLongWithDefault(
            propertyName, defaultValue);
    }

    public static String getString(final String bundleName,
        final String propertyName) {
        return new ResourceBundleUtils(bundleName).getString(propertyName);
    }

    public static String getStringWithDefault(final String bundleName,
          final String propertyName, final String defaultValue) {
        return new ResourceBundleUtils(bundleName).getStringWithDefault(
            propertyName, defaultValue);
    }

    ResourceBundle resourceBundle = null;

    String bundleName = "";

    private static final String NO_PROPERTY_FOUND_MSG =
        "Property not found, using: ";

    public ResourceBundleUtils() {
    }

    /**
     * @param resourceBundle
     */
    public ResourceBundleUtils(final ResourceBundle resourceBundle) {
        this.resourceBundle = resourceBundle;
    }

    /**
     * @param bundleName eg. x.y.z will read x/y/z.properties
     */
    public ResourceBundleUtils(final String bundleName) {
        this.bundleName = bundleName;

        try {
            resourceBundle = ResourceBundle.getBundle(bundleName);
        } catch (RuntimeException e) {
            log.debug("Could not find the resource you are looking for ("
                + bundleName + "), will use defaults.");
        }
    }

    public boolean getBoolean(final String propertyName) {
        return Boolean.parseBoolean(getString(propertyName));
    }

    public boolean getBooleanWithDefault(
              final String propertyName, final boolean defaultValue) {
        if (resourceBundle != null) {
            try {
                return Boolean.parseBoolean(getString(propertyName));
            } catch (Exception e) {
            }
        }
        // log.debug(NO_PROPERTY_FOUND_MSG +
        // bundleName + "." + propertyName + "=" + defaultValue);
        return defaultValue;
    }

    public int getInt(final String propertyName) {
        return Integer.parseInt(getString(propertyName));
    }

    public int getIntWithDefault(
            final String propertyName, final int defaultValue) {
        if (resourceBundle != null) {
            try {
                return Integer.parseInt(getString(propertyName));
            } catch (NumberFormatException e) {
                e.printStackTrace();
            } catch (Exception e) {
            }
        }
        // log.debug(NO_PROPERTY_FOUND_MSG +
        // bundleName + "." + propertyName + "=" + defaultValue);
        return defaultValue;
    }

    public long getLong(final String propertyName) {
        return Long.parseLong(getString(propertyName));
    }

    public long getLongWithDefault(
            final String propertyName, final long defaultValue) {
        if (resourceBundle != null) {
            try {
                return Long.parseLong(getString(propertyName));
            } catch (NumberFormatException e) {
                e.printStackTrace();
            } catch (Exception e) {
            }
        }
        // log.debug(NO_PROPERTY_FOUND_MSG +
        // bundleName + "." + propertyName + "=" + defaultValue);
        return defaultValue;
    }

    public double getDouble(final String propertyName) {
        return Double.parseDouble(getString(propertyName));
    }

    public String getString(final String propertyName)
        throws MissingResourceException {
        return getResourceBundle(propertyName).getString(propertyName);
    }

    private ResourceBundle getResourceBundle(final String resourceName) {
        if (resourceBundle == null) {
            throw new MissingResourceException(
                "Could not find the resource you are looking for",
                    bundleName, resourceName);
        }
        return resourceBundle;
    }

    public String getStringWithDefault(
                final String propertyName, final String defaultValue) {
        if (resourceBundle != null) {
            try {

                return resourceBundle.getString(propertyName);
            } catch (Exception e) {
            }
        }
        // log.debug(NO_PROPERTY_FOUND_MSG +
        // bundleName + "." + propertyName + "=" + defaultValue);
        return defaultValue;
    }

    public Set<String> getKeys() {
        return resourceBundle.keySet();
    }
}
