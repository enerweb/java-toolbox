package za.co.enerweb.toolbox.concurrency;

import java.util.Iterator;
import java.util.LinkedList;

import lombok.extern.slf4j.Slf4j;

/**
 * Utility for running a queue of tasks in a background thread.
 */
/*
 * XXX: maybe use a ExecutorService or a concurrent queue or something
 */
@Slf4j
public class BackgroundTaskRunner {
    private Thread worker;
    private LinkedList<Task> runnableQueue = new LinkedList<Task>();
    private volatile boolean closed = true;

    public BackgroundTaskRunner() {
    }

    public synchronized void open() {
        open(true);
    }

    public synchronized void open(final boolean startThread) {
        if (closed) {
            closed = false;
            if (startThread) {
                startThread();
            }
        }
    }

    public synchronized void startThread() {
        if (worker == null) {
            worker = new Thread(
                new Runner(),
                "Background Task Runner"
                );
            worker.setPriority(Thread.NORM_PRIORITY - 2);
            worker.setDaemon(true); // don't keep servers from shutting down
            worker.start();
        }
    }

    /**
     * If it gets swamped, it will start delaying.
     * @param task
     */
    public void invokeLater(final Task task) {
        queueTask(task, true);
    }

    private void queueTask(final Task task, final boolean later) {
        if (closed) {
            throw new IllegalStateException(
                "The Background Task Manager has been closed, "
                    + "and will not accept any more tasks.");
        }

        if (task == null) {
            throw new IllegalArgumentException("Task may not be null");
        }

        synchronized (runnableQueue) {
            if (later) {
                runnableQueue.addLast(task);
            } else {
                runnableQueue.addFirst(task); // waiting tasks to the front of
                // the queue
            }
            runnableQueue.notify();
        }
        int pendingTasks = runnableQueue.size();
        if (pendingTasks >= 100) {
            int milliseconds = pendingTasks * pendingTasks / 10;
            if (pendingTasks >= 200 && pendingTasks % 100 == 0) {
                log.debug("The background task runner currently has "
                    + pendingTasks + " tasks! Waiting " + (milliseconds / 1000)
                    + " seconds before enqueueing more.");
            }
            try {
                Thread.sleep(milliseconds);
            } catch (InterruptedException e) {
            }
        }
    }

    public Object invokeAndWait(final Task task) throws
        InterruptedException, TaskFailedException {
        synchronized (task) {
            queueTask(task, false);
            task.wait();
        }
        if (task.taskFailure != null) {
            if (task.taskFailure instanceof InterruptedException) {
                throw (InterruptedException) task.taskFailure;
            }
            throw new TaskFailedException(task.taskFailure);
        }
        return task.returnValue;
    }

    /**
     * Close the shop:
     * Stop accepting new tasks, and wait for current tasks to be processed.
     * @throws InterruptedException
     */
    public synchronized void closeAndWait() throws InterruptedException {
        closed = true;
        synchronized (runnableQueue) {
            runnableQueue.notify(); // for in case we are already waiting.
        }
        if (worker != null && worker.isAlive()) {
            worker.join();
        }
        worker = null;
    }

    public int waitingTasks() {
        synchronized (runnableQueue) {
            return runnableQueue.size();
        }
    }

    private final class Runner implements Runnable {
        public void run() {
            try {
                while (true) {
                    Task currentTask;
                    synchronized (runnableQueue) {

                        if (runnableQueue.size() == 0) {
                            if (closed) {
                                return;
                            }
                            // queue is empty so play a bit of solitaire
                            runnableQueue.wait();

                            if (closed) {
                                // notify all waiting tasks that they
                                // won't be executed!
                                // don't assume the queue is empty.
                                IllegalStateException e =
                                    new IllegalStateException(
                                        "The Background Task Manager has been "
                                            + "closed, and will not accept any "
                                            + "more tasks.");
                                for (Iterator<Task> iter =
                                    runnableQueue.iterator();
                                iter.hasNext();) {
                                    currentTask = iter.next();
                                    currentTask.taskFailure = e;
                                    currentTask.notify();
                                }
                                return;
                            }
                        }
                        currentTask =
                            runnableQueue.removeFirst();
                    }
                    synchronized (currentTask) {
                        try {
                            currentTask.returnValue =
                                (currentTask).run();
                        } catch (InterruptedException e) {
                            throw e;
                        } catch (Exception e) {
                            currentTask.taskFailure = e;
                        }
                        currentTask.notify();
                    }
                }
            } catch (InterruptedException e) {
                closed = true;
            }
        }
    }

    public static abstract class Task {
        private Exception taskFailure;
        private Object returnValue;

        public abstract Object run() throws Exception;
    }

    public static class TaskFailedException extends Exception {
        public TaskFailedException(final Throwable cause) {
            super("Background Task Failed.", cause);
        }
    }
}
