package za.co.enerweb.toolbox.os;

import lombok.extern.slf4j.Slf4j;

/**
 * a small os util
 */
@Slf4j
public final class OsUtils {
    private static final String OS;
    static {
        OS = System.getProperty("os.name");
        // log.debug("os.name: " + OS);
    }

    public static String OS_LINUX = "linux";
    public static String OS_WINDOWS = "windows";

    private OsUtils() {
    }

    /**
     * this currently gives the bittiness of the JVM, not the os! but its a good
     * guess to start with now.
     * @return
     */
    public static int detectJvmBits() {
        String arch = System.getProperty("sun.arch.data.model");
        log.debug("sun.arch.data.model: " + arch);
        if (arch != null) {
            return Integer.parseInt(arch);
        }
        arch = System.getProperty("os.arch");
        log.debug("os.arch: " + arch);
        if (arch.contains("64")) {
            return 64;
        }
        return 32;
    }

    public static boolean isWindows() {
        return getOsName().startsWith("Windows");
    }

    public static boolean isLinux() {
        return getOsName().equals("Linux");
    }

    /**
     * tries to give one of the canonical names defined in the OS_ constants
     * @return
     */
    public static String getOs() {
        if (isWindows()) {
            return OS_WINDOWS;
        }
        if (isLinux()) {
            return OS_LINUX;
        }
        return OS;
    }

    /**
     * the raw os.name parameter provided by the jvm
     * @return
     */
    public static String getOsName() {
        return OS;
    }

    /**
     * Quote text of shell i.e. " on windows and ' on the rest
     * @param text
     * @return
     */
    public static String quote(String text) {
        String q;
        if (isWindows()) {
            q = "\"";
        } else {
            q = "'";
        }
        return q + text + q;
    }
}
