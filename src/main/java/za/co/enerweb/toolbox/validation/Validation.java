package za.co.enerweb.toolbox.validation;

import static java.lang.String.format;
import za.co.enerweb.toolbox.objects.ObjectUtils;

public class Validation {

    public static void assertNotNull(String nameOfTheThingThatShouldNotBeNull,
        Object val) {
        if (val == null) {
            throw new ValidationException(nameOfTheThingThatShouldNotBeNull
                + " should not be null");
        }
    }

    public static void assertEquals(String nameOfTheThingThatShouldBeEqual,
        Object a, Object b) {
        if (!ObjectUtils.equals(a, b)) {
            throw new ValidationException(
                format("%s should be equal: '%s' != '%s'",
                    nameOfTheThingThatShouldBeEqual, a, b));
        }
    }
}
